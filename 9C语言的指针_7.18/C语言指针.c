#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
/*
int main()
{
	int a = 10;//申请4个字节
	int*  p = &a;//&a;//取a地址
	//printf("%p\n", p);//地址为16进制本质上二进制
	//printf("%p\n", &a);
	//有一种变量是用来存放地址的-指针变量
	//类型为int*
	*p = 20;//	*---解引用操作符
	printf("a = %d\n", a);//通过指针改变a的值
	return 0;
}*/
/*
int main()
{
	char ch = 'w';
	char* pc = &ch;
	//指针类型为char*
 //指针名为pc
	*pc = 'a';
	printf("ch = %c\n", ch);//ch=a
	return 0;
}*/
//指针变量的大小
int main()
{
	char ch = 'w';
	char* pc = &ch;
	*pc = 'a';
	printf(" %d\n", sizeof(pc));//ch=a
	return 0;
}