#pragma once
#define ROW 3
#define COL 3
#include<stdio.h>
#include<stdlib.h>
#include<time.h>

//声明
void Initqipan(char qipan[ROW][COL], int row, int col);
void Displayqipan(char qipan[ROW][COL], int row, int col);
void PlayerMove(char qipan[ROW][COL], int row, int col);
void PCMove(char qipan[ROW][COL], int row, int col);

//告诉我们四种游戏的状态
// 电脑赢	‘*’
// 玩家赢	‘#’
// 平局		‘Q’
// 继续		 'C'
int IsFull(char qipan[ROW][COL], int row, int col);
char whowin(char qipan[ROW][COL], int row, int col);