#define _CRT_SECURE_NO_WARNINGS
#include"game.h"
void menu()
{
	printf("***********************\n");
	printf("***1.play     0.exit****\n");
	printf("***********************\n");
}
/*
	|	|
——|—-|——
	|	|
——|—-|——
	|	|
*/
//游戏的整个算法实现-
void game()
{
	char ret = 0;
	//数组-存放走出的棋牌信息
	char qipan[ROW][COL] = { 0 };
	Initqipan(qipan, ROW, COL);//初始化棋盘
	Displayqipan(qipan, ROW, COL);//打印棋盘
	while (1)
	{
		//玩家下棋
		PlayerMove(qipan,ROW,COL);
		Displayqipan(qipan, ROW, COL);
		//判断玩家是否赢
		ret = whowin(qipan, ROW, COL);
		if (ret != 'C')
		{
			break;
		}
		//电脑下棋
		PCMove(qipan, ROW, COL);
		Displayqipan(qipan, ROW, COL);
		//判断电脑是否赢
		ret = whowin(qipan, ROW, COL);
		if (ret != 'C')
		{
			break;
		}
	}
	if (ret == '*')
	{
		printf("玩家赢\n");
	}
	else if (ret == '#')
	{
		printf("电脑赢\n");
	}
	else
	{
		printf("平局\n");
	}
}
void test()
{
	int input = 0;
	srand((unsigned int)time(NULL));
	do
	{
		menu();
		printf("请选择:");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			game();
			break;
		case 0:
			printf("游戏结束，退出游戏\n");
			break;
		default:
			printf("选择错误，请重新选择\n");
			break;
		}
	} while (input!=0);
}
int main()
{
	test();
	return 0;
}