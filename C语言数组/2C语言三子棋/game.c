#define _CRT_SECURE_NO_WARNINGS
#include"game.h"
void Initqipan(char qipan[ROW][COL], int row, int col)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < row; i++)
	{
		for (j = 0; j < col; j++)
		{
			qipan[i][j] = ' ';
		}
	}
}
void Displayqipan(char qipan[ROW][COL], int row, int col)
{
	int i = 0;
	for (i = 0; i < row; i++)
	{
		int j = 0;
		for (j = 0; j < col; j++)
		{
			//1.打印一行的数据
			printf(" %c ", qipan[i][j]);
			if (j < col - 1)//当j<col-1时候打印|，当=col不打印
				printf("|");
		}
		printf("\n");
		//2.打印分隔行
		if (i < row - 1)
		{
			for (j = 0; j < col; j++)
			{
				printf("---");
				if(j<col-1)
					printf("|");
			}
			printf("\n");
		}
	}
}
void PlayerMove(char qipan[ROW][COL], int row, int col)
{
	int x = 0;
	int y = 0;
	printf("玩家走:\n");
	while (1)
	{
		printf("请输入要下的坐标:");
		scanf("%d %d", &x, &y);
		if (x >= 1 && x <= row && y>=1 && y <= col)
		{
			if (qipan[x - 1][y - 1] == ' ')
			{
				qipan[x - 1][y - 1] = '*';
				break;
			}
			else
			{
				printf("该位置已有棋子，请重新输入");
			}
		}
		else
		{
			printf("请重新输入!");
		}
	}
}
void PCMove(char qipan[ROW][COL], int row, int col)
{
	int x = 0;
	int y = 0;
	printf("电脑走:\n");
	x = rand() % row;
	y = rand() % col;
	while (1)
	{
		if (qipan[x][y] == ' ')
		{
			qipan[x][y] = '#';
			break;
		}
	}
}

//返回1表示棋盘满了
//返回0，表示棋盘没满
int IsFull(char qipan[ROW][COL], int row, int col)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < row; i++)
	{
		for (j = 0; j < col; j++)
		{
			if (qipan[i][j] == ' ')
			{
				return 0;//没满
			}
		}
	}
	return 1;//满了
}
char whowin(char qipan[ROW][COL], int row, int col)
{
	int i = 0;
	//横三行
	for (i = 0; i < row; i++)
	{
		if (qipan[i][0] == qipan[i][1] && qipan[i][1] == qipan[i][2] && qipan[i][1] != ' ')
		{
			return qipan[i][1];
		}
	}
	//竖三列
	for (i = 0; i < col; i++)
	{
		if (qipan[0][i] == qipan[1][i] && qipan[1][i] == qipan[2][i] && qipan[1][i] != ' ')
		{
			return qipan[1][i];
		}
	}
	//两个对角线
	if (qipan[0][0] == qipan[1][1] && qipan[1][1] == qipan[2][2] && qipan[1][1] != ' ')
		return qipan[1][1];
	if (qipan[2][0] == qipan[1][1] && qipan[1][1] == qipan[0][2] && qipan[1][1] != ' ')
		return qipan[1][1];
	//判断是否平局
	if (1 == IsFull(qipan, ROW, COL))
	{
		return 'Q';
	}
	return 'C';
}