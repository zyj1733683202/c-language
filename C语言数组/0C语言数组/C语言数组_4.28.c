#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
/*
int main()
{
	int arr[10] = { 1,2,3,4,5,6 };//不完全初始化，剩下的元素默认初始化为0
	char arr2[5] = { 'a',98};//98对应的ASCII码是b
	char arr3[5] = "abc";//ok
	char arr4[] = "abcdef";
	printf("%d\n", sizeof(arr4));//7
	//sizeof 计算 arr4所占空间的大小
	printf("%d\n", strlen(arr4));//6
	return 0;
}*/

/*
int main()
{
	char arr[] = "abcdef";//[a][b][c][d][e][f][\0]
	int i = 0;
	int len = strlen(arr);
	for (i = 0; i<len; i++)
	{
		printf("%c ", arr[i]);
	}
	return 0;
}*/

/*
int main()
{
	int arr[] = { 1,2,3,4,5,6,7,8,9 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d", arr[i]);
	}
	return 0;
}*/
//一维数组在内存中的存储
/*
int main()
{
	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("&arr[%d] = %p\n", i, &arr[i]);
	}
	return 0;
}*/

//二维数组
/*
int main()
{
	int arr[3][4] = { {1,2,3},{4,5} };//三行四列的二维数组
	int i = 0;
	for (i = 0; i < 3; i++)
	{
		int j = 0;
		for (j = 0; j < 4; j++)
		{
			printf("%d ",arr[i][j]);
		}
		printf("\n");
	}
	return 0;
}*/
int main()
{
	int arr[3][4] = { {1,2,3},{4,5} };
	int i = 0;
	for (i = 0; i < 3; i++)
	{
		int j = 0;
		for (j = 0; j < 4; j++)
		{
			printf("&arr[%d][%d]=%p\n", i, j, &arr[i][j]);
		}
	}
	return 0;
}