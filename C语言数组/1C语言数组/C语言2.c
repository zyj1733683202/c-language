#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
/*
void my_strcpy(char arr1[], char arr2[])
{
	do
	{
		*arr1++ = *arr2;
	} while (*arr2++ != '\0');
}
int main()
{
	char arr1[] = "###########";
	char arr2[] = "hello world";
	my_strcpy(arr1, arr2);
	puts(arr1);
	return 0;
}*/

/*
void maopao_paixu(int arr[],int sz)
{
	int flag = 1;//假设这一趟要排序的数据已经有序
	//确定冒泡排序的趟数,趟数和元素个数有关系为n-1趟
	int i = 0;
	for (i = 0; i <sz-1; i++)
	{
		//每一趟冒泡排序
		int j = 0;
		for (j = 0; j <sz-1-i ; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				int tmp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = tmp;
				flag = 0;
			}
		}
		if (flag == 1)
		{
			break;//break不能用于if但这个if是在循环中的
		}
	}
}
int main()
{
	int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
	int i = 0;
	int sz = sizeof(arr) / sizeof(arr[0]);//在函数外面求出sz，再传递过去就行
	//对arr进行排序，排成升序
	maopao_paixu(arr,sz);//冒泡排序函数。对数组传参是传递的首元素的地址
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	return 0;
}*/

//数组名是数组首元素的地址。(有两个例外)
/*
int main()
{
	int arr[] = { 1,2,3,4,5,6,7 };
	int sz = sizeof(arr) / sizeof(arr[0]);
		//1.sizeof(数组名)	数组名表示整个数组 sizeof(数组名)
		//	计算的是整个数组的大小，单位是字节
		//2.&数组名，数组名代表整个数组，&数组名，取出的是整个
		//  数组的地址		printf("%p\n", &arr);整个数组的地址
	//printf("%p\n", arr);
	//printf("%p\n", &arr[0]);
	//printf("%d\n", *arr);//解引用打印出来首地址的数--1

	return 0;
}
*/

void maopao_paixu(int arr[], int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		int flag = 1;
		int j = 0;
		for (j = 0; j <sz-1-i ; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				int tmp = 0;
				tmp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1]=tmp;
				flag = 0;
			}
			if (flag == 1)
			{
				break;
			}
		}
	}
}
int main()
{
	int arr[]={ 9,8,7,6,5,4,3,2,1 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	maopao_paixu(arr, sz);
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	return 0;
}
