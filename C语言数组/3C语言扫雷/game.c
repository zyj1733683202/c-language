#define _CRT_SECURE_NO_WARNINGS
#include"game.h"
void Initshow(char show[ROWS][COLS], int rows, int cols,char set)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < rows; i++)
	{
		for (j = 0; j < cols; j++)
		{
			show[i][j] = set;
		}
	}
}

void Display(char show[ROWS][COLS], int row, int col)
{
	int i = 0;
	int j = 0;
	for (i = 0; i <= row; i++)//打印行数，从0开始
	{
		printf("%d ", i);
	}
	printf("\n");
	for (j = 1; j <= col; j++)
	{
		printf("%d ", j);//打印列数，行数中已有0，从1开始
		for (i = 1; i <= row; i++)//这里有外围，所以应从1开始
		{
			printf("%c ", show[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

void Setlei(char show[ROWS][COLS], int row, int col)
{
	int count = easy_game;
	while (count)
	{
		int x = rand() % row + 1;
		int y = rand() % col + 1;
		if (show[x][y] == '0')
		{
			show[x][y] = '1';
			count--;
			/*count不能放在if外部，因为生成的随机数可能重复，放在if内部就可以在遇到未重复的变换的时候才-1。
			若放在外部，遇到不等于0的；虽不执行=1；但执行了count--。*/
		}
	}
}

//'0'-'0'=0
//'1'-'0'=1
//'3'-'0'=3
int get_round_lei(char show[ROWS][COLS], int x, int y)
{
	return show[x - 1][y] +
		show[x - 1][y + 1] +
		show[x][y + 1] +
		show[x + 1][y + 1] +
		show[x + 1][y] +
		show[x + 1][y - 1] +
		show[x][y - 1] +
		show[x - 1][y - 1] - 8 * '0';//类型转换为数字
}

int Jieguo(char qipan[ROWS][COLS],char show[ROWS][COLS], int row, int col)
{
	int x = 0;
	int y = 0;
	int win = 0;
	while (win<row*col-easy_game)
	{
		printf("请输入雷的坐标");
		scanf("%d %d", &x, &y);
		if (x >= 1 && y >= 1 && x <= row && y <= col)
		{
			if (show[x][y] == '1')
			{
				printf("你被炸死了，游戏结束！\n");
				Display(show, row, col);
				break;
			}
			else
			{
				int count = 0;
				count = get_round_lei(show,x,y);
				qipan[x][y] = count + '0';//把count的数字转化为字符
				Display(qipan, row, col);//打印排查出一次雷的信息
				win++;
			}
		}
		else
		{
			printf("输入错误，请重新输入.");
		}
	}
	if (win == row * col - easy_game)
	{
		printf("排雷成功\n");
	}
}
