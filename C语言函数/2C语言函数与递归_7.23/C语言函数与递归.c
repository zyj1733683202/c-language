#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
/*
int main()
{
	printf("hehe\n");
	main();
	return 0;
}*/
//接受一个整型值(无符号)，按照顺序打印它的每一位。例如:输入1234，输出1 2 3 4.
/*
void print(int n)//1234
{
	if (n > 9)//是否两位数
	{
		print(n / 10);
	}
	printf("%d ", n % 10);
}
int main()
{
	unsigned int num = 0;
	scanf("%d", &num);//1234,不断取模
	//递归
	print(num);
	//print(1234)
	//print(123)  4
	//print(12)  3  4
	//print(1)  2  3  4,当数字只剩1位就不用拆了
	return 0;
}*/

//编写函数不允许创建临时变量，求字符串的长度。
/*
int my_strlen(char* str)
{
	int count = 0;
	while (*str != '\0')
	{
		count++;
		str++;
	}
	return count;
}
int main()
{
	char arr[] = "zxx";
	//int len = strlen(arr);//求字符串长度
	//printf("%d\n", len);
	
	//模拟实现了一个strlen函数;
	int len = my_strlen(arr);//arr是数组，数组传参，传过去的不是整个数组，而是首元素的地址
	printf("len=%d\n", len);
	return 0;
}*/
//递归的方法
/*
int my_strlen(char* str)
{
	if (*str != '\0')//*str解引用
	{
		return 1 + my_strlen(str + 1);//str + 1-x的地址
	}
	else
		return 0;
}
//把大事化小。
//my_strlen("pwh");
//1+my_strlen("wh");
//1+1+my_strlen("h");
//1+1+1+my_strlen("");
//1+1+1+0=3;
int main()
{
	char arr[] = "pwh";
	//int len = strlen(arr);//求字符串长度
	//printf("%d\n", len);

	//模拟实现了一个strlen函数;
	int len = my_strlen(arr);//arr是数组，数组传参，传过去的不是整个数组，而是首元素的地址
	printf("len=%d\n", len);
	return 0;
}*/

//求n的阶乘
/*
int Fac1(int n)
{
	int i = 0;
	int ret = 1;
	for (i = 1; i <= n; i++)
	{
		ret *= i;
	}
	return ret;
}
int main()
{
	//求n的阶乘
	int n = 0;
	int ret = 0;
	scanf("%d", &n);
	ret = Fac1(n);//循环的方式
	printf("%d\n", ret);

	return 0;
}*/
/*
int Fac2(int n)
{
	if (n <= 1)
		return 1;
	else
		return n * Fac2(n - 1);
}
int main()
{
	//求n的阶乘
	int n = 0;
	int ret = 0;
	scanf("%d", &n);
	ret = Fac2(n);//循环的方式
	printf("%d\n", ret);
	 
	return 0;
}
*/

//求第n个斐波那契数
//1 1 2 3 5 8 13 21 34 55.......(递归法效率太低)
/*
int count = 0 ;
int Fib(int n)
{
	if (n == 3)
	{
		count++;
	}//第三个斐波那契数列被重复算了很多次
	if (n <= 2)
		return 1;
	else
		return Fib(n - 1) + Fib(n - 2);
}
//效率很慢，求50要先知道49 48 ，求49得先知道48 47，求48得先知道47 46。
int main()
{
	int n = 0;
	int ret = 0;
	scanf("%d", &n);
	ret = Fib(n);
	printf("%d\n", ret);
	printf("%d", count);
	return 0;
}*/
//循环法
/*
int Fib(int n)
{
	int a = 1;
	int b = 1;
	int c = 1;//c用来存放临时的斐波那契数
	while (n > 2)
	{
		c = a + b;
		a = b;
		b = c;
		n--;
	}
	return c;
}
int main()
{
	int n = 0;
	int ret = 0;
	scanf("%d", &n);
	ret = Fib(n);
	printf("%d\n", ret);
	return 0;
}*/

void test(int n)
{
	if (n < 10000)
	{
		test(n + 1);
	}
}//栈溢出
int main()
{
	test(1);
	return 0;
}