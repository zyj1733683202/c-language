#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>

void hanoi(int n, char A, char B, char C)
{

	if (n == 1)
	{
		printf("%c -> %c\n", A, C);
	}
	else
	{
		hanoi(n - 1, A, C, B);//把n-1个盘子从A绕过C放在B

        printf("%c -> %c\n", A, C);
		hanoi(n - 1, B, A, C);//把n-1个盘子从B绕过A放在C
	}
}
int main()
{
	int n = 0;
	scanf("%d", &n);
	hanoi(n, 'A', 'B', 'C');
	return 0;
}
/*
int hanoi(int n, char x, char y, char z)
{
    int move(char x, int n, char z);
    if (n == 1)//递归出口
        move(x, 1, z);
    else
    {
        hanoi(n - 1, x, z, y);//把n-1个盘子从x绕过z放在y
        move(x, n, z);
        hanoi(n - 1, y, x, z); //把n-1个盘子从y绕过x放在z
    }
    return 0;
}
int move(char get, int n, char put)
{
    static int k = 1;
    printf("%2d:%3d # %c-->%c\n", k, n, get, put);
    if (k++ % 3 == 0)
        printf("\n");
    return 0;
}
int main()
{
    int hanoi(int n, char A, char B, char C);
    int n, counter;
    printf("请输入汉诺塔层数：");
    scanf("%d", &n);
    printf("\n");
    counter = hanoi(n, 'A', 'B', 'C');
    return 0;
}*/