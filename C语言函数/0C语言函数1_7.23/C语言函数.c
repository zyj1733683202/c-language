#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
#include<math.h>
/*
int Add(int x, int y)
{
	int z = 0;
	z = x + y;
	return z;
}
int main()
{
	int a = 10;
	int b = 20;
	int sum = Add(a, b);
	printf("%d\n", sum);
	return 0;
}*/
//strcpy函数
//#include<string.h>
/*
int main()
{
	//char* strcpy(char* destination(目的地，终点), const char* source(来源，出处，根源，起源));
	char arr1[] = "bit";
	char arr2[20] = "########";
				   //bit\0####,实际上拷贝了四个字符，但是有\0结束了打印。
	strcpy(arr2,arr1);//将arr1内容拷贝到arr2
	//strcpy-string copy	-	字符串拷贝
	//strlen-string length	-	字符串长度有关
	printf("%s\n", arr2);
	return 0;
}*/
//memset
//#include<string.h>
/*
int main()
{
	char arr[] = "hello world";
	memset(arr, '*', 5);
	printf("%s\n", arr);
	return 0;
}*/

//定义函数
/*
int get_max(int x, int y)
{
	if (x > y)
		return x;
	else
		return y;
}
int main()
{
	int a = 10;
	int b = 20;
	int max = get_max(a, b);//传递参数
	printf("max=%d\n", max);
	return 0;
}*/

//写一个函数可以交换两个整型变量的内容
/*
void Swap2(int* pa, int* pb)
{
	int tmp = 0;
	tmp = *pa;
	*pa = *pb;
	*pb = tmp;
}
int main()
{
	int a = 10;
	int b = 20;
	printf("a=%d b=%d\n", a, b);
	Swap2(&a, &b);//把地址传过去
	printf("a=%d b=%d\n", a, b);
	return 0;
}*/
//代码有缺陷，不行
/*
void Swap1(int x,int y)//void代表没有返回值
{
	int tmp = 0;
	tmp = x;
	x = y;
	y = tmp;
}  
int main()
{
	int a = 10;
	int b = 20;
	printf("a=%d b=%d\n", a, b);
	Swap(a, b);//只需要交换就可以，不需要返回
	printf("a=%d b=%d\n", a, b);
	return 0;
}
//a,b     x,y  内存空间不同，内存空间是独立的*/

//打印100-200之间的素数
/*只要从2尝试到sqrt(x)就可以，因为因数都是成对出现的，例
如100的因数有：1和100、2和50、4和25、5和20、10和10,成对的因数，
其中一个必然小于等于100的开方，另一个必然大于等于100的开方。*/
/*
int is_prime(int n)
{
	int j;
	for (j = 2; j <= sqrt(n); j++)
	{
		if (n % j == 0)
			return 0;
	}
		if (j > sqrt(n))//如果没有一个数小于这个数的开平方，则它为素数
		return 1;
}
int main()
{
	int  i = 0;
	for (i = 100; i <= 200; i++)
	{
		//判断i是否为素数
		if (is_prime(i) == 1)
		printf("%d ", i);
	}
	return 0;
}*/

//写一个函数判断一年是不是闰年
/*
int run_year(int y)
{
	if (((y % 4 == 0) && (y % 100 != 0)) || (y % 100 == 0))
		return 1;
	else
		return 0;
}
int main()
{
	int year = 0;
	scanf("%d", &year);
	if (run_year(year) == 1)
		printf("%d年是闰年", year);
	else
		printf("%d年不是闰年", year);
	return 0;
}*/

//写一个函数，实现一个整形有序数组的二分查找。
/*
	//本质上arr是个指针
 int is_cha(int arr[], int k,int size)
{
	//那么在函数体中求元素个数size就不靠谱了
	int left = 0;
	int right = size - 1;

	while (left <= right)//left <= right中间还有元素可以查找，但是大于就无法查找
	{	
		int mid = (left + right) / 2;
		if (arr[mid] > k)
		{
			right = mid - 1;
		}
		else if (arr[mid] < k)
		{
			left = mid + 1;
		}
		else
		{
			return mid;
		}
	}
	return 1;
}
int main()
{
	int arr[] = {1,2,3,4,5,6,7,8,9,10};
	int k = 7;
	int size = sizeof(arr) / sizeof(arr[0]);
	int result = is_cha(arr, k, size);//传递过去的是数组arr首元素地址
	if (result == 1)
	{
		printf("找不到指定的数字\n");
	}
	else
	{
		printf("找到了,下标是: %d\n", result);
	}
	return 0;
}*/

//写一个函数，每调用一次这个函数，就会将num的值增加1。
/*
void plus(int* p)
{
	 (*p)++;//指针是一个整体需要用括号
}
int main()
{
	int num = 0;
	plus(&num);//传地址
	printf("num=%d\n", num);
	plus(&num);
	printf("num=%d\n", num);
	plus(&num);
	printf("num=%d\n", num);
	return 0;
}*/
//链式访问
/*
int main()
{
	int len = 0;
	//1
	len = strlen("abc");
	printf("%d\n", len);
	//2
	printf("%d\n", strlen("abc"));//链式访问
	return 0;
}*/
/*
int main()
{
	printf("%d", printf("%d", printf("%d", 43)));//结果为4321
	//1			//2			 //43	调用位数:43为两位
	return 0;
}*/