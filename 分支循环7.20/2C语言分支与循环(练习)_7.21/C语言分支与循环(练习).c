#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
#include<Windows.h>
#include<stdlib.h>
//计算n的阶乘
/*
int main()
{
	int i = 0;
	int n = 0;
	int ret = 1;
	scanf("%d", &n);//不考虑溢出的情况
	for (i = 1; i <= n ; i++)
	{
		ret = ret * i;
	}
	printf("ret=%d\n", ret);
		return 0;
}*/
//计算阶乘的和
/*
int main()
{
	int n = 0;
	int ret = 1;
	int sum = 0;
	//1+2+6=9;
	for (n = 1; n <= 5; n++)
	{
		ret = ret * n;	//n的阶乘
		sum = sum + ret;
	}
	printf("sum=%d\n", sum);
	return 0;
}*/
//在一个有序数组中查找具体某个数字n
//int binsearch(int x,int v[ ],int n);
//功能是在v[0]<=v[1]<=v[2]...<=v[n-1]的数组中查找x
/*
int main()
{
	int arr[ ] = {2,4,6,8,10,12,14,16,18,20};
	int k = 12;
	//写一个代码，在arr数组(有序的)中找到
	int i = 0;
	int sz = sizeof(arr)/sizeof(arr[0]);//利用数组中所有元素所占字节计算元素个数
		for(i = 0; i < sz; i++)
		{
			if (k == arr[i])
			{
				printf("找到了，下标是:%d\n", i);
				break;
			}
		}
	if (i == sz)
		printf("找不到\n");
	return 0;
}*/
//二分法
int main()
{
	int arr[ ] = {1,2,3,4,5,6,7,8,9,10,11};
	int k = 7;
	int sz = sizeof(arr)/sizeof(arr[0]);//计算元素个数
	int left = 0;//左下标
	int right = sz - 1;//右下标

	while (left <= right)//三次循环
	{
		int mid = (left + right)/2;
		if (arr[mid] > k)
		{
			right = mid - 1;
		}
		else if (arr[mid] < k)
		{
			left = mid + 1;
		}
		else
		{
			printf("找到了，下标是: %d\n", mid);
			break;
		}
	}
		if (left > right)
		{
			printf("找不到\n");
		}
	return 0;
}
//编写代码，演示多个字符从两段移动，向中间汇聚
/*
int main()
{
	char arr1[] = "welcome to bit!!!!!!";
	char arr2[] = "####################";
	int left = 0;
	//int right = sizeof(arr1) / sizeof(arr1[0]) - 2;
	int right = strlen(arr1) - 1;
	while (left<=right)
	{
		arr2[left] = arr1[left];
		arr2[right] = arr1[right];
		printf("%s\n", arr2);
		//休息1秒
		Sleep(1000);
		left++;
		right--;
	}
	return 0;
}*/
//编写代码实现，模拟用户登录情景，并且只能登录三次
//只允许输入三次密码，如果密码正确提示登录成功，
//如果三次输错，退出程序
/*
int main()
{
	int i = 0;
	char password[20] = { 0 };
	for (i = 0; i < 3; i++)
	{
		printf("请输入密码:>");
		scanf("%s", password);
		if (strcmp(password, "123456") == 0)
			//不能用来比较两个字符串是否相等，应该使用一个库函数strcmp
			//string compare(字符串比较)的缩写
		{
			printf("登录成功\n");
			break;
		}
	}
	if (i == 3)
		printf("三次密码均错误，退出程序\n");
	return 0;
}*/
//strlen和sizeof
/*
char a[]="hello";
char b[]={'h','e','l','l','o'};
 strlen(a),strlen(b)的值分别是多少?
 
strlen是求字符串的长度，字符串有个默认的结束符\0,
这个结束符是在定义字符串的时候系统自动加上去的，就像定义数组a一样。
数组a定义了一个字符串，数组b定义了一个字符数组。
因此，strlen(a)=5,而strlen(b)的长度就不确定的，因为strlen找不到结束符。*/
/*
int main()
{
	int i = 0;
	int n = 0;
	int ret = 1;
	int sum = 0;
	for (n = 1; n <= 3; n++)
	{
		//ret=1;加入ret=1就可以
		for (i = 1; i <= n; i++)//再次进入循环时，i会被重新赋值为1
		{
			ret = ret * i;
 		}//每一次计算时ret应该从1开始;否则会有累乘的效果
		sum = sum + ret;
	}
	printf("sum=%d\n", sum);
	return 0;
}*/