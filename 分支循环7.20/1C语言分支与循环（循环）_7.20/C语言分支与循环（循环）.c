#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
/*
int main()
{
	int	i = 1;
	while(i<=10)
	{ 
		if (i == 5)
			break;
		printf("%d ", i);
		i++;
	}
	return 0;
}*/
/*
int main()
{
	int	i = 1;
	while (i <= 10)
	{
		if (i == 5)
			continue;//会继续循环下去，但是死循环
		//将不再执行continue以后的代码，而是从头开始循环
		printf("%d ", i);
		i++;
	}
	return 0;
}*/
/*
int main()
{
	int	i = 1;
	while (i <= 10)
	{	
		i++;
		if (i == 5)
			continue;//当i=5，continue执行，但不执行continue下面的代码
		//将不再执行continue以后的代码，而是从头开始循环
		printf("%d ", i);
	}
	return 0;
}*/
/*
int main()
{
	int ch = 0;

	//键入ctrl+z就可以终止循环
	//EOF - end	of	file文件结束标志 = -1
	while ((ch = getchar()) != EOF)
		//getchar()接收键盘输入一个字符
		putchar(ch);
	//如果不等于EOF打印字符
	//但是每次输入的其实是三个字符
	//所以只用键盘敲入是无法终止循环的
	return 0;
}*/
/*
int main()
{
	int ret = 0;
	int ch = 0;
	char password[20] = {0};
	printf("请输入密码:>");
	scanf("%s", password);//输入密码并存放在password的数组中
	//缓冲区还剩一个'\n'
	//可以读取一下'\n'
	//scanf只可以读取空格之前的内容
	while ((ch = getchar())!= '\n')
	//因为使用!=需要表达式，所以必须定义变量ch
	//while循环将到'\n'以及之前的内容全部读取
	//输入123456 ABCD剩余字符为‘ ’‘A’‘B’‘C’‘D’‘\n’
	//读取6次;最后有一个'\n'
	{
		;
	}
	printf("请确认(Y/N):>");
	ret = getchar();//	Y/N
	if (ret == 'Y')
	{
		printf("确认成功\n");
	}
	else
	{
		printf("放弃确认\n");
	}
	//printf("%d\n",'\n');
	return 0;
}
//C语言中的单引号用来表示字符字面量，编译为对应的ASCII码
//C语言中的双引号用来表示字符串字面量，编译为对应的内存地址
*/
/*
int main()
{
	int ch = 0;
	while ((ch = getchar()) != EOF)//只接收一个字符，打10是没用的
	{
		if (ch <= '0' || ch >= '9')
			continue;
			putchar(ch);
	}
	return 0;
}*/
//while循环的问题:三个部分分散，不易调整
/*
int main()
{
	int i = 0;
	  //初始化  判断	 调整
	for (i = 1; i <= 10; i++)
	{
		if (i == 5)
			break;//break和continue用法和while中一样
		printf("%d ",i);
	}
	return 0;
}*/
/*
int main()
{
	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
	int i = 0;
	//10次循环
	//10次打印
	//10个元素
	for (i = 0; i < 10; i++)//前闭后开区间
	//for (i = 0; i <=9; i++)也行，但不建议这么写
	{
		printf("%d ", arr[i]);
	}
	return 0;
}*/
//for循环的变种
/*
int main()
{
	int i = 0;
	int j = 0;
	for (; i < 10; i++)
	{
		for (; j < 10; j++)
		{
			printf("hehe\n");
		}
	}
	return 0;
}//当第二次执行for(i)循环时，会读取for(j)循环
//但不会打印，因为此时j已经被+到10了。而j=0又被省略了。
*/

/*
int main()
{
	//变种1
	//for (;;)
	//{
	//	printf("hehe\n");
	//}
	//变种2
	int x, y;
	for (x = 0, y = 0; x < 2 && y < 5; ++x, y++)
	{
		printf("hehe\n");
	}
	return 0;
}*/
/*
int main()
{
	int i = 0;
	int k = 0;
	for (i = 0, k = 0; k = 0; i++, k++)//k=0为赋值，0为假
		//所以一次循环也没执行，压根不执行循环
		k++;
	return 0;
}*/
//do......while循环
int main()
{
	int i = 1;
	do
	{
		if (i == 5)
			continue;
		printf("%d ", i);
		i++;
	}
	//do......while循环中的break和其他循环相投
	//continue进入死循环，不执行continue后面的代码块，直接跳到while
	while (i <= 10);
	return 0;
}

