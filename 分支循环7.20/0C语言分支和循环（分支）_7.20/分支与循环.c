#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
/*
int main()
{
	int age;
	printf("请输入年龄:");
	scanf("%d", &age);
	if (age < 18)
		printf("未成年\n");
	else if(age>=18 && age<28)
		printf("青年\n");
	else if(age>=28 && age<50)
		printf("壮年\n");
	else if(age>=50 && age<90)
		printf("老年\n");
	else
		printf("老不死\n");
	return 0;
}
*/

//;//即便只有一个分号也是，是空语句
/*
int main()
{
	int a = 0;
	int b = 2;
	if (a == 1)//为假，下面的if就不执行
		if (b == 2)
			printf("hehe\n");

		else
			printf("haha\n");
	//可以用{	}来隔离代码
	return 0;
}*/
/*
int main()
{
	int num = 4;
	if (5 == num)//一个等号"="是赋值，判断相等是"=="
	{
		printf("呵呵\n");
	}
	return 0;
}*/
/*
int main()
{
	int i = 0;
	while (i <= 100)
	{
		if (0 != i%2)
			printf("%d\t", i);
		i++;
	}
		return 0;
}

int main()
{
	int i = 1;
	while (i <= 100)
	{
		printf("%d\t", i);
		i += 2;
	}
	return 0;
}*/
//switch语句，常常用于多分支，括号内一定为整型

int main()
{
	int day = 0;
	scanf("%d", &day);
	switch (day)
	{
		case 1:
			printf("星期一\n");
			break;
		case 2:
			printf("星期二\n");
			break;
		case 3:
			printf("星期三\n");
			break;
		case 4:
			printf("星期四\n");
			break;
		case 5:
			printf("星期五\n");
			break;
		case 6:
			printf("星期六\n");
			break;
		case 7:
			printf("星期天\n");
			break;
	}
			return 0;
}
/*
int main()
{
	int day = 0;
	scanf("%d", &day);
	switch (day)
	{
	case 1:
	case 2:
	case 3:
	case 4:
	case 5:
		printf("工作日\n");
		break;
	case 6:
		printf("休息日\n");
		break;
	case 7:
		printf("休息日\n");
		break;
	}
	return 0;
}*/
/*
int main()
{
	int day = 0;
	int n = 1;
	scanf("%d", &day);
	switch (day)
	{
	case 1:
		if (n == 1)
			printf("hehe\n");//switch语句可以嵌套if语句
	case 2:
	case 3:
	case 4:
	case 5:
		printf("工作日\n");
		break;
	case 6:
		printf("休息日\n");
		break;
	case 7:
		printf("休息日\n");
		break;
	default:
		printf("输入错误\n");
		break;
	}//处理一些非法的状态,default顺序可以随便放
	return 0;
}*/
/*
int main()
{
	int n = 1;
	int m = 2;
	switch (n)//switch以n为入口
	{
		case 1:m++;//3
		case 2:n++;//2
		case 3:
			switch (n)
			{//switch允许嵌套使用
			case 1:n++;//不执行，n已经等于2了
				//当switch后面括号内的“表达式”的值与某个case后面常量表达式的值相等，那就直接执行此case后面的语句
				//switch (2)就是从第二个例子开始
			case 2:m++, n++;//4,3
				break;
			}
		case 4:m++;//5
			break;
	default:
		break;
	}
	printf("m=%d, n = %d\n", m, n);
	return 0;
}*/