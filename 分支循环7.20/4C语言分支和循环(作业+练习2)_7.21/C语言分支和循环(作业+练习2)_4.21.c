#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>
#include<string.h>
//编写程序数一下1-100的所有整数中出现多少个数字9
/*
int main()
{
	int a;
	int count = 0;
	for (a = 9; a <= 100; a++)
	{
		if (a % 10 == 9)//9  19  29  39。。。。。。 
			count++;
		if (a / 10 == 9)//90  91  92  93  94。。。。。。
			count++;//a是整数，只有9
		//两个if都要判断
	}
	printf("%d ", count);
	return 0;
}*/
//分数求和，计算1/1-1/2+1/3+-1/4+1/5-......+1/99-1/100的值
/*
int main()
{
	int i = 0;
	double sum = 0.0;
	int flag = 1;
		for (i = 1; i <= 100; i++)
		{	
			sum += flag * 1.0 / i;
			flag = -flag;//乘过之后变为负的，下次使用就是负的，再变化为正
		}
		printf("%lf ", sum);
	return 0;
}*/
//求10个整数中的最大值
/*
int main()
{
	int arr[] = {-1,-2,-3,-4,-5,-6,-7,-8,-9,-10};
	int max = arr[0];//最大值
	int i = 0;
	int sz = sizeof(arr) / sizeof(arr[0]);
	for (i = 0; i < sz; i++)
	{
		if (arr[i] > max)
		{
			max = arr[i];
		}
	}
	printf("max = %d\n", max);
	return 0;
}*/
//在屏幕上输出9*9乘法口诀表
/*
int main()
{
	int a = 1;
	for (a = 1; a <= 9; a++)
	{	
		int b = 1;
		for (b = 1; b <= a; b++)//这样可以做到更美观第1行就1个，第2行就2个
		{
			printf("%d*%d=%-2d ", a,b,a*b);//对齐
		}
		printf("\n");
	}
	return 0;
}*/
//猜数字游戏(二分)
//1.电脑会生成一个1-100之间随机数
//2.猜数字
/**/
void menu()
{
	printf("************************************\n");
	printf("***********1.play	0.exit**********\n");
	printf("************************************\n");
}
//RAND_MAX(0-32767)
void game()
{
	//1.生成一个随机数
	int ret = 0;
	int guess = 0;
	ret = rand()%100+1;//生成1-100之间随机数
	//printf("%d ", ret);
	//2.猜数字
	while (1)
	{
		printf("请猜数字:>");
		scanf("%d", &guess);
		if (guess < ret)
		{
			printf("猜小了\n");
		}
		else if (guess > ret)
		{
			printf("猜大了\n");
		}
		else
		{
			printf("恭喜你，猜对了！\n");
			break;
		}
	}
}
int main()
{
	int input = 0;
	srand((unsigned int)time(NULL));
	//time括号内是指针，但不想用就写NULL
	//srand设置一个随机起点，给一个时刻变化的值，例如时间
	//拿时间戳来设置随机数的生成起始点
	//time_t time(time_t *timer)
	//time_t//本质上是个整型
	//为什么要放在主函数内，放在game函数中玩一次设置一次

	do //do while循环至少执行一次
	{
		menu();
		printf("请选择>:");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			game();//猜数字游戏
			break;
		case 0:
			printf("退出游戏\n");
			break;
		default:
			printf("选择错误\n");
			break;
		}
	} while (input);//input不确定，input取决于用户输入，input为真则重复case1，为0则退出
	return 0;
}
//goto语句
/*
int main()
{
	printf("hello pwb\n");
	goto again;
	printf("你好\n");
again:
	printf("hehe\n");
	return 0;
}*/
/*
int main()
{
	//shutdown	-s	-t 60
	//system()-执行系统命令的
	char input[20] = { 0 };
	system("shutdown /s /t 60");
	while (1)
	{
		printf("请注意，你电脑将在一分钟内关机，如果输入：我是猪，则取消关机\n请输入>:");
		scanf("%s", input);
		if (strcmp(input, "我是猪") == 0)//比较两个字符串-strcmp()
		{
			system("shutdown  /a");
			break;
		}
	}
	return 0;
}*/