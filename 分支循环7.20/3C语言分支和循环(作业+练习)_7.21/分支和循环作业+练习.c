#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<math.h>
/*
int main()
{
	int x = 3;
	int y = 3;
	switch (x%2)
	{
	case 1:
		switch (y)
		{
		case 0:printf("first");
		case 1:printf("second");
			break;
		default:printf("hello");
		}
		//case 1下面没有break 继续执行
	case 2:
		printf("third");
	}
	return 0;
}*/
//3个数排序
/*
int main()
{
	int a;
	int b;
	int c;
	printf("请输入三个数：\n");
	scanf("%d %d %d", &a,&b,&c);
	if (a > b && b > c)
		printf("%d %d %d", a, b, c);

	else if (a > b && b < c)
		printf("%d %d %d", a, c, b);

	else if (b > a && a > c)
		printf("%d %d %d", b, a, c);

	else if (b > c && a < c)
		printf("%d %d %d", b, c, a);

	else if (c > a && b < a)
		printf("%d %d %d", c, a, b);
	else
		printf("%d %d %d", c, b, a);
	
	return 0;
}*/
/*
int main()
{
	int a=0;
	int b=0;
	int c=0;
	scanf("%d,%d,%d", &a, &b, &c);
	//算法的实现
	//a中放最大值
	//b次之
	//c中放最小值
	if (a < b)
	{
		int tmp = a;
		a = b;
		b = tmp;
	}
	if (a < c)
	{
		int tmp = a;
		a = c;
		c = tmp;
	}
	if (b < c)
	{
		int tmp = b;
		b = c;
		c = tmp;
	}
	printf("%d %d %d\n", a, b, c);
	return 0;
}*/
//打印1-100之间3的倍数的数
/*
int main()
{
	int a = 0;
	int i = 0;
	for (i = 1; i <=100; i++)
	{
		a++;
		if (a % 3 == 0)
		{
			printf("%d ", a);
		}
	}
	return 0;
}*/
//给定两个数，求这两个数的最大公约数
//辗转相除法
/*
int main()
{
	int a;
	int b;
	int m;
	scanf("%d %d", &a, &b);
	if (a % b == 0)
	{
		printf("最大公约数为：%d", b);
		
	}
	else
	{
		m = a % b;
		if(b%m==0)
		printf("最大公约数为：%d", m);
	}
	return 0;
}*/
/*
int main()
{
	int m = 24;
	int n = 18;
	int r = 0;
	scanf("%d %d", &m, &n);
	while (m % n!=0)
	{
		r = m % n;
		m = n;
		n = r;
	}
	printf("最大公约数为：%d\n", n);
	return 0;
}*/
//打印1000-2000年之间的闰年
/*
int main()
{
	int year = 0;
	int count = 0;
	for (year = 1000; year <= 2000; year++)
	{
		//判断year是否为闰年
		//1.能被4整除且不能被100整除的是闰年
		//2.能被400整除是闰年
		
		if (year % 4 == 0 && year % 100 != 0)
		{
			printf("%d ", year);
			count++;
		}
		else if (year % 400 == 0)
		{
			printf("%d ", year);
			count++;
		}
	}
	printf("\n总共有闰年=%d ",count);
	return 0;
}*/
/*
int main()
{
	int year;
	int count = 0;
	for(year=1000;year<=2000;year++)
	{ 
		if (((year % 4 == 0) && (year % 100 != 0)) || (year % 100 == 0))
		{
			printf("%d ", year);
			count++;
		}
	}
	printf("\n总共有闰年=%d ", count);
	return 0;
}*/
//打印100-200之间的素数
//进行优化
/*
int main()
{
	//素数：只有公因数1和自身
	//1.试除法
	int a;
	int i;
	int count=0;
 	for (a = 100; a <= 200; a++)
	{
		for (i = 2; i < a; i++)
		{
			if (a % i == 0)
			{
				break;
			}	
		}
		if (a == i)
		{
			count++;
			printf("%d ", a);
		}
	}
	printf("\ncount= %d \n", count);
	return 0;
}*/
/*
int main()
{
	int a;
	int i;
	int count = 0;
	//偶数不可能是素数，直接去掉偶数
	//公约数中至少有一个数字<=开平方
	for (a = 101; a <= 200; a+=2)
	{
		for (i = 2; i < sqrt(a); i++)
		{
			if (a % i == 0)
			{
				break;
			}
		}
		if (i >sqrt(a))
		{
			count++;
			printf("%d ", a);
		}
	}
	printf("\ncount= %d \n", count);
	return 0;
}*/
int main()
{
	int a = 0, b = 0;
	for (a = 1, b = 1; a <= 100; a++)
	{
		if (b >= 20)
			break;
		if (b % 3 == 1)
		{
			b = b + 3;
			continue;
		}
		b = b - 5;
	}
	printf("%d\n", a);
	return 0;
}
