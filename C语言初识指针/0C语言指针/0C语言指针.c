#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
/*
int main()
{
	//int a = 10;//在内存中开辟一块空间
	//int* p = &a;//指针变量，这里我们对变量a，取出它的地址，可以使用&操作符。
				//将a的地址存放在p变量中，p就是一个指针变量。
	//printf("%d\n", sizeof(char*));
	//printf("%d\n", sizeof(short*));
	//printf("%d\n", sizeof(int*));
	//printf("%d\n", sizeof(double*));
	int a = 0x11223344;//在内存中开辟一块空间
	int* pa = &a;
	*pa = 0;
	//char* pc = &a;
	//printf("%p\n", pa);
	//printf("%p\n", pc);
	return 0;
}*/
//指针 + -整数
/*
int main()
{
	int a = 0x11223344;
	int* pa = &a;
	char* pc = &a;
	printf("%p\n", pa);
	printf("%p\n", pa+1);
	printf("%p\n", pc);
	printf("%p\n", pc+1);
	return 0;
}*/
/*
int main()
{
	int arr[10] = { 0 };//一个元素占用四个字节，但64位计算机一个内存空间是8个字节，所以两个元素占一个内存空间
	int * p = arr;//数组名，首元素地址
	int i = 0;
	for (i = 0; i < 10; i++)
	{
		*(p + i) = 1;
	}
	return 0;
}*/

//野指针
//指针未初始化
/*
int main()
{
	int* p;//局部的指针变量，就被初始化随机值
	*p = 20;
	return 0;
}*/
//指针越界
/*
int main()
{
	int arr[10] = { 0 };
	int* p = arr;
	int i = 0;
	for (i = 0; i < 12; i++)
	{
		*(p++)=i;
	}//指针指向的范围超出数组arr的范围时，p就是野指针
	return 0;
}*/
//指针指向内存空间被释放
/*
int* test()
{
	int a = 10;//出这个函数就销毁
	//a创建了一个地址空间0x0012ff44，内容为10
	return &a;
	//地址返回给指针p	0x0012ff44，但是函数空间已经被释放
}
int main()
{
	int* p = test(); 
	*p = 20;//那么该语句此时访问的地址不是当前程序的，已经变成别人的了
	return 0;
}
*/

