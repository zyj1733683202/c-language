#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
//初始化
/*
int main()
{
	int a = 10;
	int* pa = &a;//初始化
	int* p = NULL;//当不知道该初始化什么时候为NULL
	return 0;
}*/
//将指针置为NULL
/*
int main()
{
	int a = 10;
	int* pa = &a;
	*pa = 20;

	pa = NULL;//pa不指向任何空间了
}*/
//指针不为空指针才能使用
/*
int main()
{
	int a = 10;
	int* pa = &a;
	*pa = 20;

	pa = NULL;//pa不指向任何空间了
	if (pa != NULL)
	{
		*pa = 20;//指针不为空指针才能使用它
	}
}*/

//指针的加法
/*
int main()
{
	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
	int i = 0;
	int sz = sizeof(arr) / sizeof(arr[0]);
	int* p = arr;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", *p);
		p = p + 1;
	}
	return 0;
}*/
/*
int main()
{
	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
	int i = 0;
	int sz = sizeof(arr) / sizeof(arr[0]);
	int* p = arr;
	for (i = 0; i < 5; i++)
	{
		printf("%d ", *p);
		p = p + 2;
	}
	return 0;
}*/

/*
int main()
{
	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
	int i = 0;
	int sz = sizeof(arr) / sizeof(arr[0]);
	int* p = &arr[9];
	for (i = 0; i < 5; i++)
	{
		printf("%d ", *p);
		p -= 2;
	}
	return 0;
}*/

//指针的减法
/*
int main()
{
	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
	printf("%d\n",&arr[9] - &arr[0]);
	//指针减去指针是指针中间元素的个数,但是不能两个不同空间的指针减
	return 0;
}*/
//用指针减法求数组元素个数
/*
int my_strlen(char* str)
{
	char* start = str;
	char* end = str;
	while (*end != '\0')
	{
		end++;
	}
	return end - start;
}
int main()
{
	//strlen-求字符串长度
	//递归-模拟实现了strlen-计数器的方式1.	递归的方式2
	char arr[] = "bit";//b  i  t  \0
	int len=my_strlen(arr);
	printf("%d\n", len);
	return 0;
}*/

//指针的关系运算
/*
#define N_VALUES 5
int main()
{
	float value[N_VALUES];
	float* vp;
	for (vp = &value[N_VALUES]; vp > &value[0];)
	{
		*--vp = 0;
	}
	return 0;
}*/

//指针与数组
/*
int main()
{
	int arr[10] = { 0 };
	printf("%p\n", arr);//地址-首元素的地址
	printf("%p\n", &arr[0]);//
	printf("%p\n", &arr);//取出的是整个数组的地址
	//1.&arr-&数组名-数组名不是首元素的地址-数组名表示整个数组-&数组名取出的是整个数组的地址
	//2.sizeof(arr)-sizeof(数组名)-数组名表示的整个数组-sizeof(数组名)计算的是整个数组的大小
	return 0;
}*/
/*
int main()
{
	int arr[10] = { 0 };
	printf("%p\n", arr);
	printf("%p\n", arr+1);
	printf("%p\n", &arr[0]);
	printf("%p\n", &arr[0]+1);
	printf("%p\n", &arr);
	printf("%p\n", &arr+1);
	return 0;
}*/
/*
int main()
{
	int arr[10] = { 0 };
	int* p = arr;
	int i = 0;
	for (i = 0; i < 10; i++)
	{
		printf("%p		===		%p\n", p+i,&arr[i]);
	}
	return 0;
}*/
/*
int main()
{
	int arr[10] = { 0 };
	int* p = arr;
	int i = 0;
	for (i = 0; i < 10; i++)
	{
		*(p + i) = i;
	}
	for (i = 0; i < 10; i++)
	{
		printf("%d ", arr[i]);
	}
	return 0;
}*/
//二级指针
/*
int main()
{
	int a = 10;
	int* pa = &a;
	int** ppa = &pa;//ppa就是二级指针
	**ppa = 20;
	printf("%d\n", *pa);
	printf("%d\n", a);
	return 0;
}*/

/*
int main()
{
	int a = 10;
	int b = 20;
	int c = 30;
//	int* pa = &a;
//	int* pb = &b;
//	int* pc = &c;
	//整型数组 - 存放整型
	//字符数组 - 存放字符
	//指针数组 - 存放指针
	//int arr[10];
	int* arr2[3] = { &a,&b,&c };//整型指针数组
	int i = 0;
	for (i = 0; i < 3; i++)
	{
		printf("%d ", *(arr2[i]));
	}
	return 0;
}*/

//int* test()
//{
//	int a = 10;
//	return &a; 
//}
//int main()
//{
//	int* p = test();
//	printf("hehe\n");
//	printf("%d\n", *p);//因为test函数被销毁，*p野指针，非法访问。虽然打印出10,但是就只是保留了10，没有被其他空间所覆盖
//	//如果前面加上printf("hehe\n"),原来的地址可能被覆盖，*p就是printf函数的地址printf("%d\n",printf("hehe\n")),也就是字符个数
//	return 0;
//}
