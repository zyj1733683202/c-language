#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
//指针复习
/*
int main() 
{
	int a = 10;//申请4个字节空间
	//printf("%p\n", &a);
	int* p = &a;//*告诉我们 p是一个变量-指针变量
	//语法形式
	//printf("%p\n", p);
	*p = 20;//*	-	解引用操作符/间接访问操作符
	printf("a = %d\n", a);
	return 0;
}*/
/*
int main()
{
	double d = 3.14;
	double* pd = &d;//32-4或64-8
	printf("%d\n", sizeof(pd));
	//不管什么指针类型，大小都是只和操作系统位数有关
	//	*pd = 5.5;
	//	printf("%lf\n", d);
	//	printf("%lf\n", *pd);
	return 0;
}*/
//创建一个结构体类型
/*
struct  Book
{
	//打印结构体信息
	char   name[20];//C语言程序设计
	short  price;//55
};
int main()
{
	//利用结构体类型创建一个该类型的变量
	struct  Book  b1 = { "C语言程序设计",55 };
	struct  Book* pb = &b1;
	//利用pb打印出我的书名和价格
	//.		结构体变量.成员
	//->	结构体指针->成员
	printf("%s\n", pb->name);//与下行代码相同
	printf("%d\n", pb->price);
	//printf("%s\n", (*pb).name);
	//printf("%d\n", (*pb).price);



	//printf("书名:%s\n", b1.name);
	//printf("价格:%d元\n", b1.price);
	//b1.price = 15;
	//printf("修改后的价格:%d元\n", b1.price);
	//.为结构成员访问操作符
	return 0;
}*/
struct  Book
{
	//打印结构体信息
	char   name[20];//C语言程序设计
	short  price;//55
};
int main()
{
	struct Book b1 = { "C语言程序设计",55 };
	strcpy(b1.name,"C++");
	//strcpy-string copy -字符串拷贝-库函数string.h
	//strcpy(b1.目的地 数据拷贝到哪里,"拷贝的内容")    
	printf("%s\n", b1.name);
	return 0;
}