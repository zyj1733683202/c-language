#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
 int Add(int x, int y)//接受传递的a，b
{
	int z=	x + y;//将x+y存入变量z
	return z;//算出来z后带回z，返回z是整型，那么函数类型是整型
	//返回z返回给sum，此时sum就为30
}//函数体

//选择语句
/*
int main()
{
	int input = 0;
	printf("加入光荣的进化\n");
	printf("你要好好学习吗？(1/0)>: ");
	scanf("%d", &input);//1 /0
	if (input == 1)
		printf("走向人生巅峰\n");
	else
		printf("回家种地\n");
	return 0;
}*/
//while循环
/*
int main()
{
	int line = 0;
	printf("加入光荣的进化吧。");
	while (line < 20000) 
	{
		printf("敲代码%d\n",line);
		line++;
	}
	if (line >= 20000);
	printf("走向人生巅峰\n");
	 	return 0;
}*/
//函数
/**/
int main()
{
	int num1 = 10;
	int num2 = 20;
	int sum = 0;
	int a = 100;
	int b = 200;
	//sum=num1+num2;
	sum = Add(num1, num2);//函数
	//sum=a+b;
	sum = Add(a, b);
	sum = Add(2, 3);
	printf("sum = %d\n", sum);
	return 0;
}
//f(x) = 2 * x + 1;
//f(x, y) = x + y;