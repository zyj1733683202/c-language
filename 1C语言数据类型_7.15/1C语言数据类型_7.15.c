/*
数据类型

char		//字符数据类型
short		//短整型
int			//整型
long		//长整型
longlong	//更长的整型
float		//单精度浮点数
double		//双精度浮点数
*/
#include<stdio.h>
//char - 字符类型
//%d - 打印整型
//%c - 打印字符
//%f - 打印浮点型数字-打印小数
//%p - 以地址的形式打印
//%x - 打印16进制数字
//%o ....

int main()
{
	printf("%d\n", sizeof(char));		//1个字节
	printf("%d\n", sizeof(short));		//2个字节
	printf("%d\n", sizeof(int));		//4个字节
	printf("%d\n", sizeof(long));		//4	或者 8个字节；
	printf("%d\n", sizeof(long long));	//8个字节
	printf("%d\n", sizeof(float));		//4个字节
	printf("%d\n", sizeof(double));		//8个字节

	short int age = 20;// 人的年龄没有那么大，用short int节省内存空间
	return 0;

	//char ch = 'A';//内存申请空间,创建变量
	//printf("%c\n", ch);//%c-打印字符格式的数据
	//以字符的形式打印ch
	//20;
	
	 
	//short int-短整型
	//int-整型
	//long-长整型
	 

	//int age = 20;
	//printf("%d\n",age);//%d-打印整型十进制数据
	//long num = 100;
	//printf("%d", num);


	//float f = 5.0;
	//printf("%f\n", f);//float默认为6位小数

	//double d = 3.14;
	//printf("%lf\n",d);//打印双精度
	//return 0;
}
