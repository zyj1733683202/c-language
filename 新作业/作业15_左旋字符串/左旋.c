#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<assert.h>
#include<string.h>
//左旋字符串中的k个字符
//先考虑左旋一个字符，然后放入循环即可，从旋转一个字符开始思考
// 
//1.暴力求解法
/*
void left_move(char *arr,int k,int len)
{
	assert(arr != NULL);
	int i = 0;
	for (i = 0; i < k; i++)
	{
		//左旋转一个字符
		//1
		char tmp = *arr;
		//2
		int j = 0;
		for (j = 0; j <len-1 ; j++)
		{
			*(arr + j) = *(arr + j + 1);
		}
		//3
		*(arr + len - 1) = tmp;
	}
}
int main()
{
	char arr[] = "ABCDEF";
	int len = strlen(arr);
	int k =3;
	left_move(arr, k, len);
	printf("%s", arr);
	return 0;
}*/
//2.三步翻转法
// 逆序，先看左旋几个字符，分开求
//abcdef
//bafedc
//cdefab
/*
void reverse(char* left, char* right)
{
	assert(left != NULL);
	assert(right != NULL);
	while (left < right)
	{
		char tmp = *left;
		*left = *right;
		*right = tmp;
		left++;
		right--;
	}
}
void left_move(char* arr, int k)
{
	assert(arr!=NULL);
	int len = strlen(arr);
	assert(k <= len);
	reverse(arr, arr + k - 1);//逆序左边
	reverse(arr + k, arr + len - 1);//逆序右边
	reverse(arr, arr + len - 1);//逆序整体
}
int main()
{
	char arr[] = "ABCDEF";
	int k = 2;
	left_move(arr, k);
	printf("%s", arr);
	return 0;
}*/


//写一个函数，判断一个字符串是否为另外一个字符串旋转之后的字符串
//例如给定s1=ABCD和s2=ACBD，返回0
//AABCD左旋一个字符得到ABCDA
//AABCD左旋两个字符得到BCDAA
//AABCD右旋一个字符得到DAABC
//1.
/*
void reverse(char* left, char* right)
{
	assert(left != NULL);
	assert(right != NULL);
	while (left < right)
	{
		char tmp = *left;
		*left = *right;
		*right = tmp;
		left++;
		right--;
	}
}
void left_move(char* arr, int k)
{
	assert(arr != NULL);
	int len = strlen(arr);
	assert(k <= len);
	reverse(arr, arr + k - 1);//逆序左边
	reverse(arr + k, arr + len - 1);//逆序右边
	reverse(arr, arr + len - 1);//逆序整体
}
*/
void left_move(char* arr, int k, int len)
{
	assert(arr != NULL);
	int i = 0;
	for (i = 0; i < k; i++)
	{
		//左旋转一个字符
		//1
		char tmp = *arr;
		//2
		int j = 0;
		for (j = 0; j < len - 1; j++)
		{
			*(arr + j) = *(arr + j + 1);
		}
		//3
		*(arr + len - 1) = tmp;
	}
}
int is_left_move(char* s1, char* s2)
{
	int len = strlen(s1);
	int i = 0;
	for (i = 0; i < len; i++)
	{
		left_move(s1, 1, len);//一个个的逆序，挨个排查
		int ret = strcmp(s1, s2);
		if (ret == 0)
			return 1;
	}
	return 0;
}

int main()
{
	char arr1[] = "abcdef";
	char arr2[] = "cdefab";
	int ret = is_left_move(arr1, arr2);
	if (ret == 1)
	{
		printf("YES\n");
	}
	else
	{
		printf("NO\n");
	}
	return 0;
}
//在str1中追加一个str1，这样里面就包含了所有的逆序可能性，再排查str2是否是其中一个子串
/*
int is_left_move(char* str1, char* str2)
{
	int len1 = strlen(str1);
	int len2 = strlen(str2);
	//如果长度不相等直接返回0
	if (len1 != len2)
		return 0;
	//1.在str1字符串中追加一个str1字符串
	//strcat(str1,str1);//err
	strncat(str1, str1, 6);//abcdefabcdef
	//2.判断str2指向的字符串是否是str1指向的字符串的子串
	//strstr-找子串的
	char* ret = strstr(str1, str2);
	if (ret == NULL)
	{
		return 0;
	}
	else
	{
		return 1;
	}
	return 0;
}
int main()
{
	char arr1[30] = "abcdef";
	char arr2[] = "cdefab";
	int ret = is_left_move(arr1, arr2);
	if (ret == 1)
	{
		printf("YES\n");
	}
	else
	{
		printf("NO\n");
	}
	return 0;
}*/
