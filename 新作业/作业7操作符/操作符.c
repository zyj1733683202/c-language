#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//元音和非元音
/*
int main()
{
	char ch = 0;
	char arr[10] = { 'A','a','E','e','O','o','U','u','I','i'};
	while (scanf("%c", &ch) != EOF)
	{
		if (ch == '\n')//忽略输入的换行符
		{
			continue;
		}
		int flag = 0;//限制条件输出非元音
		int i = 0;
		for (i = 0; i < 10; i++)
		{
			if (ch == arr[i])
			{
				printf("Vowel\n");
				flag = 1;
				break;
			}
		}
		if (flag!=1)
		{
			printf("Consonant\n");
		}
	}
	return 0;
}*/
//先输入两个数字指定矩阵大小，然后输入元素，判断两个矩阵，相等输出YES
/*
int main()
{
	int arr1[50][50] = { 0 };
	int arr2[50][50] = { 0 };
	int i, j, n, m, flag = 1;
	scanf("%d%d", &n, &m);
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < m; j++) 
		{
			scanf("%d", &arr1[i][j]);
		}
	}
	for (i = 0; i < n; i++) {
		for (j = 0; j < m; j++) 
		{
			scanf("%d", &arr2[i][j]);
			if (arr1[i][j] != arr2[i][j])
			{
				flag = 0;
			}
		}
	}
	if (flag)
	{
		printf("Yes\n");
	}
	else
	{
		printf("No\n");
	}
	return 0;
}
*/

//将一个矩阵行列转置（行列互换）
int main()
{
	int arr1[50][50] = { 0 };
	int arr2[50][50] = { 0 };
	int m,n,i,j = 0;
	scanf("%d %d", &m, &n);
	for (i = 0; i < m; i++)//行
	{
		for (j = 0; j < n; j++)//列
		{
			scanf("%d", &arr1[i][j]);//键入二维数组的值
		}
	}
	int tmp = 0;
	tmp = m;//交换
	m = n;
	n = tmp;
	for (i = 0; i < m; i++)//行
	{
		for (j = 0; j < n; j++)//列
		{
			arr2[i][j] = arr1[j][i];
		}
	}
	for (i = 0; i < m; i++)
	{
		for (j = 0; j < n; j++)
		{
			printf("%d ", arr2[i][j]);
		}
		printf("\n");
	}
	return 0;
}