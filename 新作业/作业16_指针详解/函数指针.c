#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
//有一个数字矩阵，矩阵的每行从左到右是递增的，杨氏矩阵
//矩阵从上到下是递增的，请编写程序在这样的矩阵中查找某个数字是否存在。
/*
int find_number(int arr[3][3],int *px,int *py,int k)
{
	int x = 0;
	int y = *py - 1;
	while (x <= *px - 1 && y >= 0)
	{
		if (arr[x][y] > k)
		{
			y--;//排查该列
		}
		else if (arr[x][y] < k)
		{
			x++;//排查另一行
		}
		else
		{
			*px = x;
			*py = y;
			return 1;
		}
	}
}
int main()
{
	int arr[3][3] = { {1,2,3},{4,5,6},{7,8,9} };
	int k = 0;
	scanf("%d", &k);
	int x = 3;
	int y = 3;
	int ret=find_number(arr,&x,&y,k);//传址应该取地址
	if (ret == 1)
	{
		printf("找到了\n");
		printf("下标是:%d %d", x, y);
	}
	else
	{
		printf("找不到\n");
	}
	return 0;
}*/
//练习使用库函数，qsort排序各种类型的数据
struct Stu
{
	char name[20];
	int age;
};
int cmp_int(const void * e1,const void* e2)
{
	return *(int*)e1 - *(int*)e2;
}
int cmp_float(const void* e1, const void* e2)
{
	return ((int)(*(float*)e1 - *(float*)e2));//浮点数-浮点数仍然是浮点数
}
int cmp_struct_age(const void* e1, const void* e2)
{
	return ((struct Stu*)e1)->age - ((struct Stu*)e2)->age;
}
int cmp_struct_name(const void* e1, const void* e2)
{
	return strcmp(((struct Stu*)e1)->name, ((struct Stu*)e2)->name);
}
void int1()
{
	int arr[10] = { 2,1,3,4,5,6,7,8,9,10 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	qsort(arr, sz, sizeof(arr[0]), cmp_int);
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
	return 0;
}
void float2()
{
	float f[] = { 9.0,8.0,7.0,6.0,5.0,4.0 };
	int sz = sizeof(f) / sizeof(f[0]);
	qsort(f, sz, sizeof(f[0]), cmp_float);
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%f ", f[i]);
	}
	printf("\n");
	return 0;
}
void struct_age3()
{
	struct Stu s[3] = { {"pwh",20},{"hsx",21},{"th",19} };
	int sz = sizeof(s) / sizeof(s[0]);
	qsort(s, sz, sizeof(s[0]), cmp_struct_age);
	int i = 0;
	for (i = 0; i < 3; i++)
	{
		printf("%d ", s[i].age);
	}
	printf("\n");
	return 0;
}
void struct_name4()
{
	struct Stu s[3] = { {"pwh",20},{"hsx",21},{"th",19} };
	int sz = sizeof(s) / sizeof(s[0]);
	qsort(s, sz, sizeof(s[0]), cmp_struct_name);
	int i = 0;
	for (i = 0; i < 3; i++)
	{
		printf("%s ", s[i].name);
	}
	printf("\n");
	return 0;
}
int main()
{
	int1();
	float2();
	struct_age3();
	struct_name4();
}
