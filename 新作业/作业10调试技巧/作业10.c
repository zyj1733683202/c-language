#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
//调整奇数偶数顺序
// 输入一个整数数组，实现一个函数
//来调整该数组中数字的顺序使得数组中所有的奇数位于数组的前半部分，
//所有偶数位于数组的后半部分

//方法1
/*
void adjust(int *arr,int len)
{
	int left = 0;
	int right = len - 1;
	while (left < right)
	{
		if(arr[left] % 2 == 0 && arr[right] % 2 != 0)
		{
			int tmp = 0;
			tmp = arr[left];
			arr[left] = arr[right];
			arr[right] = tmp;
			left++;
			right--;
		}
		else if(arr[left] % 2 != 0 && arr[right] % 2 == 0)
		{
			left++;
			right--;
			continue;
		}
		else if(arr[left] % 2 != 0 && arr[right]%2!=0)
		{
			left++;
		}
		else
		{
			right--;
		}
	}
}
*/
//方法2
/*
void adjust(int* arr, int len)
{
	int left = 0;
	int right = len - 1;
	while (left < right)
	{
		if (arr[left] % 2 == 0)
		{
			int tmp = 0;
			while (1)
			{
				if (arr[right] % 2 == 1)
				{
					tmp = arr[left];
					arr[left] = arr[right];
					arr[right] = tmp;
					break;
				}
				else//两边都为偶数
				{
					right--;
				}
			}
		}
		else
		{
			left++;
		}
	}
}*/
//方法3
/*
void adjust(int* arr, int sz)
{
	int left = 0;
	int right = sz - 1;
	while (left < right)
	{
		//从左向右找偶数
		while ((left < right) && arr[left] % 2 == 1)
		{
			left++;
		}
		//从右边找奇数
		while ((left < right) && arr[right] % 2 == 0)
		{
			right--;
		}
		if (left < right)
		{
			int tmp = arr[left];
			arr[left] = arr[right];
			arr[right] = tmp;
		}
	}
}
*/
//主函数
/*
int main()
{
	int arr[11];
	int i = 0;
	for (i = 0; i < 11; i++)
	{
		scanf("%d", &arr[i]);
	}
	int len = sizeof(arr) / sizeof(arr[0]);
	adjust(arr,len);
	for (i = 0; i < 11; i++)
	{
		printf("%d ", arr[i]);
	}
	return 0;
}*/
//数组越界访问
#include <stdio.h>
int main()
{
	int i = 0;
	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
	for (i = 0; i <= 12; i++)
	{
		arr[i] = 0;
		printf("hello bit\n");
	}
	return 0;
}