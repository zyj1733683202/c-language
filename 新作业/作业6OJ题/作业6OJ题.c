#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//转换6进制
/*
void Binary_conversion(int n)
{
	if (n >= 6)
	{
		Binary_conversion(n / 6);
	}
	printf("%d", n % 6);
}
int main()
{
	int n = 0;
	scanf("%d", &n);
	Binary_conversion(n);
	return 0;
}*/
//电梯问题
/*
#include<stdio.h>
int main()
{
	int n = 0;
	scanf("%d", &n);
	int min = 0;
	if (n < 12)
	{
		printf("%d", 2);
	}
	else
	{
		min = (n / 12) * 4 + 2;
		printf("%d", min);
	}
}
*/
//最大公约数和最大公倍数
/*
#include<stdio.h>
int main()
{
	long int m, n = 0;
	scanf("%d %d", &m, &n);
	long int a = m;
	long int b = n;
	long int sum = 0;
	while (a % b)
	{
		long int tmp = a % b;
		a = b;
		b = tmp;//此时b为最大公约数
	}
	long int min = (m * n) / b;//最小公倍数
	sum = min + b;
	printf("%ld", sum);
	return 0;
}*/
//字符大小写判断
/*
#include<stdio.h>
int main()
{
	char a = 0;
	while (scanf("%c", &a) != EOF)
	{
		getchar();
		if ((a >= 'A' && a <= 'Z') || (a >= 'a' && a <= 'z'))
		{
			printf("YES\n");
		}
		else
		{
			printf("NO\n");
		}
	}
	return 0;
}*/

//空心正方形
/*
#include<stdio.h>
int main()
{
	int n = 0;//行数，也是*的数量
	while (scanf("%d", &n) != EOF)
	{
		int i = 0;
		for (i = 0; i < n; i++)//行数
		{
			int j = 0;
			if ((i == 0) || (i == n - 1))
			{
				for (j = 0; j < n; j++)//每一行
				{
					printf("*");
					printf(" ");
				}
			}
			else
			{
				printf("*");
				for (j = 0; j < 2 * n - 3; j++)
				{
					printf(" ");
				}
				printf("*");
			}
			printf("\n");
		}
	}
}*/
//7个公务员，最高分和最低分
/*
#include<stdio.h>
int main()
{
	int arr[7] = { 0 };
	int i = 0;
	int max = 0;
	int min = 100;
	for (i = 0; i < 7; i++)
	{
		scanf("%d ", &arr[i]);
		if (max < arr[i])
		{
			max = arr[i];
		}
		if (min > arr[i])
		{
			min = arr[i];
		}
	}
	float sum = 0;
	for (i = 0; i < 7; i++)
	{
		sum += arr[i];
	}
	sum = sum - max - min;
	float average = (sum / 5) * 1.00;
	printf("%.2f ", average);
	return 0;
}*/
//三角形判断
/*
#include<stdio.h>
int main()
{
	int a, b, c = 0;
	while (scanf("%d %d %d", &a, &b, &c) != EOF)
	{
		if (a + b > c && a + c > b && b + c > a)
		{
			if (a == b && b == c)
			{
				printf("Equilateral triangle!\n");
			}
			else if (a != b && a != c && b != c)
			{
				printf("Ordinary triangle!\n");
			}
			else
			{
				printf("Isosceles triangle!\n");
			}
		}
		else
		{
			printf("Not a triangle!\n");
		}
	}
	return 0;
}*/