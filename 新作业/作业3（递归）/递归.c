#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<assert.h>
#include<string.h>
//打印整数的每个位
/*
void print_number(int a)
{
	if (a > 9)
	{
		print_number(a / 10);
	}
	printf("%d ", a % 10);
}
int main()
{
	int a = 0;
	scanf("%d", &a);
	print_number(a);
	return 0;
}*/

//n的阶乘
//递归法
/*
int Fac(int n)
{
	if (n == 1)
	{
		return 1;
	}
	else
	{
		return n * Fac(n - 1);
	}
}
int main()
{
	int n = 0;
	scanf("%d", &n);
	int ret = Fac(n);
	printf("%d", ret);
}*/
//循环法
/*
int Fac2(int n)
{
	int ret = 1;
	int i = 0;
	for (i = 1; i <= n; i++)
	{
		ret *= i;
	}
	return ret;
}
int main()
{
	int n = 0;
	scanf("%d", &n);
	int ret = Fac2(n);
	printf("%d", ret);
	return 0;
}*/

//模拟实现strlen
//递归法
/*
int my_strlen(char* arr)
{
	assert(arr != NULL);
	if (*arr)
	{
		return 1 + my_strlen(arr + 1);
	}
	else
	{
		return 0;
	}
}
int main()
{
	char arr[] = "abcdef";
	int len = my_strlen(arr);
	printf("%d", len);
	return 0;
}*/
//循环法
/*
int my_strlen(char* arr)
{
	int count = 0;
	assert(arr != NULL);
	while (*arr)
	{
		count++;
		arr++;
	}
	return count;
}
int main()
{
	char arr[] = "adwswcws";
	int len = my_strlen(arr);
	printf("%d", len);
}*/

//编写一个函数 reverse_string(char * string)（递归实现）
//实现：将参数字符串中的字符反向排列，不是逆序打印。
/*
int my_strlen(char* arr)
{
	int count = 0;
	while (*arr++)
	{
		count++;
	}
	return count;
}
void reverse_string(char* arr)
{
	int len = my_strlen(arr);
	char* tmp = *arr;
	*arr = *(arr + len - 1);
	*(arr + len - 1) = '\0';//这样总体长度不断-1,当替换完毕前面时，进行递归时就会释放出tmp存储的值
	//从arr+1处读取
	//fbcde0 my_strlen长度为4，tmp存储为a
	//fecd00 my_strlen长度为2，tmp存储为b
	//fed000 my_strlen长度为0，返回，tmp存储为c
	if (my_strlen(arr + 1) >= 2)
	{
		reverse_string(arr + 1);//从arr+1处读取
	}
	*(arr + len - 1) = tmp;//从fed000开始，依次返回c，b，a
}
int main()
{
	char arr[] = "abcdef";
	reverse_string(arr);
	printf("%s", arr);
	return 0;
}*/

/*
int my_strlen(char* arr)
{
	int count = 0;
	while (*arr++)
	{
		count++;
	}
	return count;
}
void reverse_string(char* arr,int left,int right)
{
	assert(arr != NULL);
	if (left <= right)
	{
		char tmp = *(arr+left);
		*(arr+left) = *(arr + right);
		*(arr + right) = tmp;
		reverse_string(arr, left + 1, right - 1);
	}
}
int main()
{
	char arr[] = "abcdef";
	int len = my_strlen(arr);
	int left = 0;
	int right = len - 1;
	reverse_string(arr, left, right);
	printf("%s", arr);
}*/

//写一个递归函数DigitSum(n)，输入一个非负整数，返回组成它的数字之和
/*
int DigitSum(int n)
{
	int sum = 0;
	int m = 0;
	if (n)
	{
		m = n % 10;
		n = n / 10;
		sum = m + DigitSum(n);
	}
	return sum;
}
int main()
{
	int n =0;
	scanf("%d", &n);
	int sum = DigitSum(n);
	printf("%d", sum);
}
*/

//编写一个函数实现n的k次方，使用递归实现。
/*
double my_Pow(int n, int k)
{
	if (k > 0)
	{
		return n * my_Pow(n, k-1);
	}
	else if (k == 0)
	{
		return 1;
	}
	else
	{
		return (1.0 / my_Pow(n, -k));
	}
}
int main()
{
	int n = 0;
	int k = 0;
	scanf("%d%d", &n, &k);
	double ret = my_Pow(n, k);
	printf("ret=%lf\n", ret);
	return 0;
}
*/

//递归和非递归实现求第n个斐波那契数列
//递归
/*
int Fib(int n)
{
	if (n <= 2)
	{
		return 1;
	}
	else
	{
		return Fib(n - 1) + Fib(n - 2);
	}
}
int main()
{
	int n = 0;
	scanf("%d", &n);
	int ret = Fib(n);
	printf("%d", ret);
}*/
//非递归
int Fib(int n)
{
	int a = 1;
	int b = 1;
	int i = 0;
	int c = 0;
	if (n <= 2)
	{
		return 1;
	}
	else
	{
		for (i = 0; i < n-2; i++)
		{
			c = a + b;
			a = b;
			b = c;
		}
		return c;
	}
}
int main()
{
	int n = 0;
	scanf("%d", &n);
	int ret = Fib(n);
	printf("%d", ret);
}