#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//十进制转六进制
/*
int Binary_conversion(int n)
{
	if (n>=6)
	{
		Binary_conversion(n / 6);
	}
	printf("%d", n % 6);
}
int main()
{
	int n = 0;
	scanf("%d", &n);
	Binary_conversion(n);
	return 0;
}*/
//小乐乐学校教学楼的电梯前排了很多人，他的前面有n个人在等电梯。
//电梯每次可以乘坐12人，每次上下需要的时间为4分钟（上需要2分钟，下需要2分钟）。
//请帮助小乐乐计算还需要多少分钟才能乘电梯到达楼上。（假设最初电梯在1层）
/*
int main()
{
	int n = 0;
	scanf("%d", &n);
	int min = 0;
	if (n < 12)
	{
		printf("%d", 2);
	}
	else
	{
		min = (n / 12) * 4 + 2;
		printf("%d", min);
	}
}
*/

//输入两个数，输出一个正整数，为n和m的最大公约数与最小公倍数之和。
/*
int main()
{
	long int m, n = 0;
	scanf("%d %d", &m, &n);
	long int a = m;
	long int b = n;
	long int sum = 0;
	while (a % b)
	{
		long int tmp = a % b;
		a = b;
		b = tmp;//此时b为最大公约数
	}
	long int min = (m * n) / b;//最小公倍数
	sum = min + b;
	printf("%ld", sum);
	return 0;
}*/


//判断字母大写还是小写
/*
int main()
{
	char a = 0;
	while (scanf("%c", &a)!=EOF)
	{
		getchar();
		if ((a >= 'A' && a <= 'Z') || (a >= 'a' && a <= 'z'))
		{
			printf("YES\n");
		}
		else
		{
			printf("NO\n");
		}
	}
	return 0;
}*/

//打印由*组成的空心正方形
/*
int main()
{
	int n = 0;//行数，也是*的数量
	while (scanf("%d", &n) != EOF)
	{
		int i = 0;
		for (i = 0; i < n; i++)//行数
		{
			int j = 0;
			if ((i == 0) || (i == n - 1))
			{
				for (j = 0; j < n; j++)//每一行
				{
					printf("*");
					printf(" ");
				}
			}
			else
			{
				printf("*");
				for (j = 0; j < 2 * n - 3; j++)
				{
					printf(" ");
				}
				printf("*");
			}
			printf("\n");
		}
	}
}*/

//公务员面试现场打分。有7位考官，从键盘输入若干组成绩，每组7个分数（百分制），去掉一个最高分和一个最低分，输出每组的平均成绩。
//一行，输入7个整数（0~100），代表7个成绩，用空格分隔。
//一行，输出去掉最高分和最低分的平均成绩，小数点后保留2位，每行输出后换行。
/*
int main()
{
	int arr[7] = { 0 };
	int i = 0;
	int max = 0;
	int min = 100;
	for (i = 0; i < 7; i++)
	{
		scanf("%d ", &arr[i]);
		if (max < arr[i])
		{
			max = arr[i];
		}
		if (min > arr[i])
		{
			min = arr[i];
		}
	}
	float sum = 0;
	for (i = 0; i < 7; i++)
	{
		sum += arr[i];
	}
	sum = sum - max - min;
	float average = (sum / 5)*1.0;
	printf("%.2f ", average);
	return 0;
}*/

//三角形判断（输入三角形的三条边，并且判断是否可以，判断三角形类型）
int main()
{
	int a, b, c = 0;
	while (scanf("%d %d %d", &a, &b, &c) != EOF)
	{
		if (a + b > c && a + c > b && b + c > a)
		{
			if (a == b && b == c)
			{
				printf("Equilateral triangle!\n");
			}
			else if (a != b && a != c && b != c)
			{
				printf("Ordinary triangle!\n");
			}
			else
			{
				printf("Isosceles triangle!\n");
			}
		}
		else
		{
			printf("Not a triangle!\n");
		}
	}
	return 0;
}