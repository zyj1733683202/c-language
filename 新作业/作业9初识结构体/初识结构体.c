#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
//逆序字符串
/*
Reverse(char* arr)
{
	int len = strlen(arr);
	int left = 0;
	int right = len - 1;
	while (left < right)
	{
		char tmp = 0;
		tmp = *(arr + left);
		*(arr + left) = *(arr + right);
		*(arr + right) = tmp;
		left++;
		right--;
	}
}
int main()
{
	char arr[] = "I am a student";
	Reverse(arr);
	printf("%s", arr);
}*/

//打印菱形
/*
int main()
{
	int line = 0;
	scanf("%d", &line);
	int i = 0;
	for (i = 0; i < line; i++)
	{
		//打印空格
		int j = 0;
		for (j = 0; j < line - 1 - i; j++)
		{
			printf(" ");
		}
		//打印*
		for (j = 0; j < 2 * i + 1; j++)
		{
			printf("*");
		}
		printf("\n");
	}
	//打印下半部分
	for (i = 0; i < line - 1; i++)
	{
		//打印空格
		int j = 0;
		for (j = 0; j <= i; j++)
		{
			printf(" ");
		}
		//打印*
		for (j = 0; j < 2 * (line - 1 - i) - 1; j++)
		{
			printf("*");
		}
		printf("\n");
	}
}*/
//喝汽水，1瓶汽水1元，2个空瓶可以换一瓶汽水，给20元，可以多少汽水
int main()
{
	int money = 0;//总共有多少钱
	int price = 0;//汽水单价
	printf("请输入您有多少钱");
	scanf("%d", &money);
	printf("请输入汽水的单价");
	scanf("%d", &price);
	int empty = 0;//空瓶数
	empty = money / price;
	int sum = empty;//喝掉的汽水数
	while (empty >= 2)
	{
		sum += empty / 2;
		empty = empty / 2 + empty % 2;
	}
	printf("您一共可以兑换%d瓶汽水", sum);
}