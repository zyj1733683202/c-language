#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<assert.h>
//模拟实现库函数strlen
/*
unsigned int my_strlen(const char* src)
{
	assert(src != NULL);
	int count = 0;
	while (*src++)
	{
		count++;
	}
	return count;
}
int main()
{
	const char arr[] = "abcdef";
	int len = my_strlen(arr);
	printf("%d", len);
	return 0;
}*/

//模拟实现strcpy
void my_strcpy(char* dest, const char* src)
{
	assert(dest != NULL);
	assert(src != NULL);
	while (*dest++ = *src++)
	{
		;
	}
}
int main()
{
	char arr1[] = "abcdef";
	char arr2[] = "xxxxxx";
	my_strcpy(arr2, arr1);
	printf("%s", arr2);
	return 0;
}