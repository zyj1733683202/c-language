#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include<math.h>
//猜数字
/*
void menu()
{
	printf("*****************************\n");
	printf("***1.开始游戏   2.退出游戏***\n");
	printf("*****************************\n");
}
void game()
{
	int m = 0;
	int ret = rand() % 100+1;
	while (1)
	{
		printf("开始猜数字:\n");
		scanf("%d", &m);
		if (m > ret)
		{
			printf("猜大了\n");
		}
		else if (m < ret)
		{
			printf("猜小了\n");
		}
		else
		{
			printf("恭喜你猜对了！\n");
			break;
		}
	}
}
int main()
{
	int a = 0;
	srand((unsigned int)time(NULL));
	do 
	{
		menu();
		printf("请选择:");
		scanf("%d", &a);
		switch (a)
		{
		case 1:
			game();
			break;
		default:
			printf("选择错误，请重新输入!\n");
			break;
		}
	} while (a!=2);
	
	return 0;
}
*/
//判断素数
/*
int is_prime_number(int a)
{
	int i = 0;
	for (i = 2; i <=sqrt(a); i++)
	{
		if (a % i == 0)
			return 0;
	}
	if (i > sqrt(a))
	{
		return 1;
	}
}
int main()
{
	int i = 0;
	for (i = 101; i <= 200; i += 2)
	{
		if (is_prime_number(i) == 1)
			printf("%d ", i);
	}
}*/
//判断闰年
/*
int is_leap_year(int year)
{
	if ((year % 4 == 0) && year % 100 != 0 || year % 400 == 0)
		return 1;
	else
		return 0;
}
int main()
{
	int year = 0;
	printf("请输入年份:");
	scanf("%d", &year);
	if (is_leap_year(year) == 1)
	{
		printf("该年是闰年");
	}
	else
	{
		printf("该年不是闰年");
	}
}*/
//交换两个整数的内容
/*
void swap(int* pa, int* pb)
{
	int tmp = 0;
	tmp = *pa;
	*pa = *pb;
	*pb = tmp;
}
int main()
{
	int a = 10;
	int b = 20;
	swap(&a, &b);
	printf("%d,%d", a, b);
}*/
//打印乘法口诀表，口诀表的行数和列数自己指定
void multiplication_table(int a)
{
	int i = 0;
	for (i = 1; i <= a; i++)
	{
		int j = 0;
		for (j = 1; j <= i; j++)
		{
			printf("%2d*%2d=%2d ", i, j, i * j);
		}
		printf("\n");
	}
}
int main()
{
	int a = 0;
	scanf("%d", &a);
	multiplication_table(a);
}