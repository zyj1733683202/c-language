#define _CRT_SECURE_NO_WARNINGS 1
#include"game.h"
void menu()
{
	printf("请选择:\n");
	printf("***************************\n");
	printf("***1.play         2.exit***\n");
	printf("***************************\n");
}
/*
	|	|
——|—-|——
	|	|
——|—-|——
	|	|
*/
void game()
{
	char board[ROW][COL] = { 0 };
	initialize(board, ROW, COL);//初始化棋盘
	display(board, ROW, COL);//打印棋盘
	char ret = 0;
	while (1)
	{
		playermove(board, ROW, COL);//玩家移动
		display(board, ROW, COL);//打印
		ret = whowin(board, ROW, COL);
		if (ret != 'C')
		{
			break;
		}
		pcmove(board, ROW, COL);//电脑走
		display(board, ROW, COL);//打印
		ret = whowin(board, ROW, COL);
		if (ret != 'C')
		{
			break;
		}
	}
		if (ret == '*')
		{
			printf("玩家获胜\n");
		}
		else if (ret == '#')
		{
			printf("电脑获胜\n");
		}
		else
		{
			printf("平局\n");
		}
}
int main()
{
	int input = 0;
	srand((unsigned int)time(NULL));
	do
	{
		menu();
		scanf("%d", &input);
 		switch (input)
		{
		case 1:
			game();
			break;
		case 2:
			printf("退出游戏");
			break;
		default:
			printf("输入错误，请重新输入");
		}
	} while (input != 2);
	return 0;
}