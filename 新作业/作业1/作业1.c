#define _CRT_SECURE_NO_WARNINGS 1
#include <stdlib.h>
#include<stdio.h>
#include<assert.h>
#include<math.h>
//打印100-200之间的素数
/*
int main()
{
	int i = 0;;
	for (i = 101; i <= 200; i += 2)//素数必然是奇数
	{
		int j = 0;
		for (j = 2; j < sqrt(i); j++)//因数必然是成对出现的，只用除到开平方即可
		{
			if (i % j == 0)
			{
				break;
			}
		}
		if (j>sqrt(i))
		{
			printf("%d ", i);
		}
	}
	return 0;
}*/


/*
void compare(int a, int b)
{
	int max = 0;
	if (a > b)
	{
		max = a;
	}
	else
	{
		max = b;
	}
	printf("%d", max);
}
int main()
{
	int a = 0;
	int b = 0;
	scanf("%d %d", &a, &b);
	compare(a, b);
	return 0;
}*/
//时间转换
/*
void time(int a)
{
	if (0 < a < 100000000)
	{
		int hour = a / 3600;
		int minute = a % 3600 / 60;
		int seconds = a % 3600 % 60;
		printf("hour=%d minute=%d seconds=%d", hour, minute, seconds);
	}
	else
	{
		printf("请重新输入");
	}
}
int main()
{
	int seconds = 0;
	scanf("%d", &seconds);
	time(seconds);
	return 0;
}
*/
//求平均数并保留一位小数
/*
int main()
{
	int a, b, c, d, e;
	scanf("%d %d %d %d %d", &a, &b, &c, &d, &e);
	float avarage = (a + b + c + d + e) / 5.0;
	printf("%.1f", avarage);
	return 0;
}*/
//给定两个整数a和b(-1000<a,b<10000)，计算a除以b的整数商和余数
/*
int main()
{
	int a, b = 0;
	scanf("%d %d", &a, &b);
	printf("%d %d", a / b, a % b);
;	return 0;
}*/

//排序
/*
int compare(const void* a, const void* b)
{
	return -(*(int*)a - *(int*)b);
}
int main()
{
	int a, b, c = 0;
	scanf("%d %d %d", &a, &b, &c);
	int arr[3] = { a,b,c };
	int sz = sizeof(arr) / sizeof(arr[0]);
	qsort(arr, sz, sizeof(arr[0]), compare);
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
}*/
/*
void bubble_sort(int* arr,int sz)
{
	assert(arr != NULL);
	int i = 0;
	for (i = 0; i < sz-1; i++)
	{
		int j = 0;
		for (j = 0; j < sz -i- 1; j++)
		{
			if (arr[j] < arr[j + 1])
			{
				int tmp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = tmp;
			}
		}
	}
}
int main()
{
	int a, b, c = 0;
	scanf("%d %d %d", &a, &b, &c);
	int arr[3] = { a,b,c };
	int sz = sizeof(arr) / sizeof(arr[0]);
	bubble_sort(arr,sz);
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	return 0;
}*/
//3的倍数
/*
#include <stdio.h>
int main()
{
	int i = 0;
	for (i = 1; i <= 100; i++)
	{
		if (i % 3 == 0)
		{
			printf("%d ", i);
		}
	}
	return 0;
}*/

//求最大公约数
/*
int main()
{
	int m, n = 0;
	scanf("%d %d", &m, &n);
	while (m%n!=0)
	{
		int tmp = n;
		n = m % n;
		m = tmp;
	}
	printf("%d", n);
}*/
//打印1000年到2000年之间的闰年
/*
int main()
{
	int i = 0;
	int count = 0;
	for (i = 1000; i <= 2000; i++)
	{
		if (i % 4 == 0 && i % 100 != 0)
		{
			printf("%d ", i);
		}
		else if (i % 400 == 0)
		{
			printf("%d ", i);
		}
	}
	return 0;
}*/

//编写程序计算1到100的整数中出现了多少个数字9
/*
int main()
{
	int count = 0;
	int i = 0;
	for (i = 9; i <= 99; i++)
	{
		if ((i / 10) == 9)
		{
			count++;
		}
		else if((i % 10) == 9)
		{
			count++;
		}
	}
	printf("%d", count);
	return 0;
}*/

//计算1/1-1/2+1/3-1/4+1/5...+1/99-1/100的值，并打印出结果
/*
int main()
{
	int i = 0;
	float sum1 = 0;
	float sum2 = 0;
	for (i = 1; i <= 100; i += 2)
	{
		sum1 += 1.0 / i;
	}
	for (i = 2; i <= 100; i += 2)
	{
		sum2 +=- 1.0 / i;
	}
	float sum = 0;
	sum = sum1 + sum2;
	printf("%f", sum);
}*/

//求10个整数中的最大值
/*
int find_max(int *arr,int sz)
{
	int i = 0;
	int max = arr[0];
	for(i=1;i<sz-1;i++)
	{
		if (max <= *(arr+i))
		{
			max = *(arr +i);
		}
	}
	return max;
}
int main()
{
	int arr[10] = { 20,8,3,50,9,11,12,13,2,14};
	int sz = sizeof(arr) / sizeof(arr[0]);
	int max = find_max(arr,sz);
	printf("%d", max);
	return 0;
}*/

//打印9*9乘法表
/*
int main()
{
	int i, j = 0;
	for (i = 1; i <= 9; i++)
	{
		for (j = 1; j <= i; j++)
		{
			printf("%d*%d=%d ", j, i, j * i);
		}
		printf("\n");
	}
	return 0;
}*/

//编写代码在一个整形有序数组中查找具体的某个数,找到的话，打印下标，找不到打印找不到
int find_number(int arr[], int sz,int k)
{
	int left = 0;
	int right = sz - 1;

	while (left <= right)
	{
		int mid = (left + right) / 2;
		if (arr[mid] > k)
		{
			right = mid - 1;
		}
		else if (arr[mid] < k)
		{
			left = mid + 1;
		}
		else
		{
			return mid;
		}
	}
	return 1;
}
int main()
{
	int arr[10] = { 2,4,6,8,10,12,14,16,18,20 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	int k = 0;
	scanf("%d", &k);
	int ret = find_number(arr,sz,k);
	if (ret == 1)
	{
		printf("找不到");
	}
	else
	{
		printf("找到了,下标是: %d\n", ret);
	}
	return 0;
}