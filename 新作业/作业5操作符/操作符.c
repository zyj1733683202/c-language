#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//求两个数二进制中不同位的个数
/*
int main()
{
	int count = 0;
	int m, n = 0;
	scanf("%d %d", &m, &n);
	int a = m ^ n;
	int sz = sizeof(int) * 8;
	int i = 0; 
	for (i = 0; i < sz; i++)
	{
		//a =a>>i;不能这么写，a并没有返回原始值，//1+2+3右移三次却移动了6位
		if (1==(a & 1))//if(1==(a>>i)&1)这样写也可以，下面的a=a>>1去掉即可
		{
			count++;
		}
		a = a >> 1;
	}
	printf("%d", count);
	return 0;
}*/

//打印整数二进制的奇数位和偶数位
/*
int main()
{
	int a = 0;
	scanf("%d", &a);
	int sz = sizeof(int) * 8;
	int i = 0;
	printf("奇数行:");
	for (i = 0; i < sz; i+=2)
	{
		
		if (1 == ((a >> i) & 1))
		{
			printf("1");
		}
		else
		{
			printf("0");
		}
	}
	printf("\n");
	printf("偶数行:");
	for (i = 1; i < sz; i += 2)
	{
		if (1 == ((a >> i) & 1))
		{
			printf("1");
		}
		else
		{
			printf("0");
		}
	}
	return 0;
}
*/

//写一个函数返回参数二进制中的1的个数

//方法1
/*
int howmuch_1(int a)
{
	int sz = sizeof(int) * 8;
	int i = 0;
	int count = 0;
	for (i = 0; i < sz; i++)
	{
		if (1 == ((a >> i) & 1))
		{
			count++;
		}
	}
	return count;
}
int main()
{
	int a = 0;
	scanf("%d", &a);
	int count = howmuch_1(a);
	printf("共有%d个1", count);
	return 0;
}*/
//方法2
/*
int count_one_bit(int n)
{
	int count = 0;
	while (n)
	{
		n = n & (n - 1);
		count++;
	}
	return count;
}
int main()
{
	int a = 0;
	scanf("%d", &a);
	int count = count_one_bit(a);
	printf("共有%d个1", count);
	return 0;
}*/

//不允许创建临时变量，交换两个整数的内容
int main()
{
	int a, b = 0;
	scanf("%d %d", &a, &b);
	a = a ^ b;
	b = a ^ b;
	a = a ^ b;
	printf("%d %d", a, b);
	return 0;
}