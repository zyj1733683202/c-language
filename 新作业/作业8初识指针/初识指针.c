#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include<math.h>
//size of返回值类型为unsigned int 在和其他数进行运算要算术转换
/*
int i;
int main()
{
    i--;
    if (i > sizeof(i))
    {
        printf(">\n");
    }
    else
    {
        printf("<\n");
    }
    return 0;
}*/
//C语言中，0为假，非0即为真。
//全局变量，没有给初始值时，编译其会默认将其初始化为0。
//i的初始值为0，i--结果 - 1，i为整形，sizeof(i)求i类型大小是4，按照此分析来看，结果应该选择B，但是sizeof的返回值类型实际为无符号整形，因此编译器会自动将左侧i自动转换为无符号整形的数据， - 1对应的无符号整形是一个非常大的数字，超过4或者8，故实际应该选择A


//写一个函数打印arr数组的内容，不使用数组的下标，使用指针
/*
void print(int* arr,int sz)
{
    int i = 0;
    for (i = 0; i < sz; i++)
    {
        printf("%d ", *arr++);
    }
}
int main()
{
    int arr[5] = { 0 };
    int sz = sizeof(arr) / sizeof(arr[0]);
    int i = 0;
    for (i = 0; i < sz; i++)
    {
        scanf("%d", &arr[i]);
    }
    print(arr,sz);
    return 0;
}*/

//计算求和
//求Sn = a + aa + aaa + aaaa + aaaaa的前5项之和，其中a是一个数字

int total(int a, int m)
{
    int sum = 0;
    int ret = 0;
    while (m--)
    {
        ret = ret * 10 + a;
        sum += ret;
    }
    return sum;
}
int main()
{
    int a, m, Sn = 0;
    scanf("%d %d", &a, &m);
    Sn = total(a, m);
    printf("%d", Sn);
    return 0;
}


//求出0-100000之间的所有"水仙花数"并输出
//“水仙花数”是指一个n位数，其各位数字的n次方之和确好等于该数本身，如:153＝1 ^ 3＋5 ^ 3＋3 ^ 3，则153是一个“水仙花数”。
/*
int main()
{
    int i = 0;
    for (i = 0; i < 10000; i++)
    {
        int tmp = i;//临时变量，互不干扰
        int n = 0;//位数
        while (tmp > 0)
        {
            tmp = tmp / 10;
            n++;
        }
        tmp = i;
        int count = n;//计算几次
        int sum = 0;
        while (count--)
        {
            sum = sum + pow(tmp % 10, n);
            tmp = tmp / 10;
        }
        if (sum == i)
        {
            printf("%d ", i);
        }
    }
    return 0;
}*/