#define _CRT_SECURE_NO_WARNINGS
/*C语言变量与常量*/
#include<stdio.h>
/*
int num2 = 20;//全局变量-定义在{}外
			  //{}代码块
int main()
{
	//年龄
	//20
	//short age = 20;//向内存申请2个字节-16bit位，用来存放20
	//float weight = 95.6f;//向内存申请4个字节，存放小数

	
	int num = 10;//局部变量定义在代码块内部
	return 0;
}*/

/*
#include<stdio.h>

int global = 2021;//全局变量
int main()
{
	int local = 2018;//局部变量
	int global = 2022;//局部变量
	//但建议全局变量和局部变量名字不要相同，容易误会有bug
	//当局部变量和全局变量的名字相同，局部变量优先
	printf("global=%d\n", global);
	return 0;
}
//是可以这么定义的，一个是全局变量一个是局部
*/

/*
int main()
{
	//计算两个数的和；
	int num1 = 0;//向内存申请了4字节空间,有编号即为地址
	int num2 = 0;
	int sum = 0;
	//输入数据-使用输入函数scanf；
	scanf("%d%d", &num1, &num2);//&-取地址符号
	//C语言语法规定，变量要定义在当前代码快的最前面
	sum = num1 + num2;
	printf("sum = %d\n", sum);
	return 0;
}
*/
/*
int main()
{	int num = 0;
	{
		printf("num=%d\n", num);
	}
	return 0;
}*/
/*
int global = 2020;

void test()
{
	printf("test()--%d\n", global);
}
int main()
{
	test();
	printf("%d\n", global);
	return 0;
}*/

/*
int main()
{
	extern int g_val;//extern声明外部符号
	printf("g_val=%d\n", g_val);
	return 0;
}
*/

/*
int main()
{
	{
		int a = 10;
		printf("a=%d\n", a);//ok
	}//进入大括号生命周期开始，出大括号生命周期结束，无法使用
	printf("a=%d\n", a);//error
	return 0;
}*/

/*int main()
{
	//const-常属性
	//const int n = 10;
	//n是变量，但是又有常属性，所以我们说n是常变量
	//int arr[n] = { 0 };//无法这样使用
	//n = 20;
	//const修饰常变量
	//constant恒定不变的，连续不断的，不变的

	//const int num = 4;
	//num本质属性还是一个变量，不过具有了常属性
	//美籍华人，本质上还是个中国人
	//printf("%d\n", num);
	//num = 8;
	//printf("%d\n", num);

	//3;//字面常量
	//100;
	//3.14;
	return 0;
}

*/

/*
//#define定义的标识符常量
#define MAX 10

int main()
{
	int arr[MAX] = { 0 };
	//数组内应是常量
	printf("%d\n",MAX );
	return 0;
}*/

//枚举常量
//枚举--一一列举

//性别:男,女,保密
//三原色:红,黄,蓝

//枚举关键-enum
/*
enum Sex
{
	MALE,
	FEMALE,
	SECRET
};//这三个就是枚举常量
//MALE,FEMALE,SECRET - 枚举常量
int main()
{
	enum Sex s = FEMALE;
	printf("%d\n", MALE);	//0
	printf("%d\n", FEMALE);	//1
	printf("%d\n", SECRET);	//2
	return 0;
}*/
enum Color
{
	RED,
	YELLOW,
	BLUE
};
int main()
{
	enum Color color = BLUE;
	color = YELLOW;
	//color变量是可以改的
	// 	   但BLUE YELLOW本身不能改变的
	//	   BLUE = 6;//err
	//color为变量
	printf("%d\n", color);
	return 0;
}