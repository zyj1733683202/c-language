#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//类型大小
/*
int main()
{
	int a=sizeof(short);
	printf("The size of short is %d bytes.\n", a);
	int b = sizeof(int);
	printf("The size of int is %d bytes.\n", b);
	int c = sizeof(long);
	printf("The size of long is %d bytes.\n", c);
	int d = sizeof(long long);
	printf("The size of long long is %d bytes.\n", d);
}*/
//十进制整数1234对应的八进制和十六进制（字母大写），用空格分开，并且要求，在八进制前显示前导0，在十六进制数前显示前导0X。
/*
int main() 
{
	int val = 1234;
	
	//printf可以使用使用格式控制串“%o”、“%X”分别输出
	//八进制整数和十六进制整数，并使用修饰符“#”控制前导显示
	
	printf("%#o %#X\n",val, val);
	return 0;
}*/

//BoBo写了一个十六进制整数ABCDEF，他问KiKi对应的十进制整数是多少。
//十六进制整数ABCDEF对应的十进制整数，所占域宽为15。
/*
int main()
{
	int a = 0XABCDEF;
	printf("%15d", a);
}*/

//KiKi写了一个输出“Hello world!”的程序，BoBo老师告诉他printf函数有返回值，你能帮他写个程序输出printf(“Hello world!”)的返回值吗？
/*
int main() 
{
	//使用ret保存printf的返回值
	int ret = printf("Hello world!");
	printf("\n");
	printf("%d\n", ret);
	return 0;
}*/

//输入一个字符，用它构造一个三角金字塔
/*
int main()
{
	char a = 0;
	scanf("%c", &a);
	int i = 0;
	for (i = 0; i < 5; i++)
	{
		int j = 0;
		for (j = 4; j > i; j--)
		{
			printf(" ");
		}
		for (j = 0; j <= i; j++)
		{
			printf("%c",a);
			if (i > 0)
			{
				printf(" ");
			}
		}
		printf("\n");
	}
	return 0;
}*/
//转换以下ASCII码为对应字符并输出他们。73, 32, 99, 97, 110, 32, 100, 111, 32, 105, 116, 33
/*
int main()
{
	int i = 0;
	int arr[] = { 73,32,99,97,110,32,100,111,32,105,116,33 };
	for (i = 0; i < 12; i++)
	{
		printf("%c,", arr[i]);
	}
}
*/


//输入一个人的出生日期（包括年月日），将该生日中的年、月、日分别输出。
//输入只有一行，出生日期，包括年月日，年月日之间的数字没有分隔符。
//输出三行，第一行为出生年份，第二行为出生月份，第三行为出生日期。输出时如果月份或天数为1位数，需要在1位数前面补0。
/*
通过scanf函数的%m格式控制可以指定输入域宽，输入数据域宽（列数），
按此宽度截取所需数据；通过printf函数的%0格式控制符，输出数值时指定左面不使用的空位置自动填0。
*/


/*
int main()
{
	int both = 0;
	int year = 0, month = 0, day = 0;
	scanf("%4d%2d%2d", &year, &month, &day);
	//使用%0可以填充前导0
	printf("year=%d\n", year);
	printf("month=%02d\n", month);
	printf("date=%02d\n", day);
	return 0;
}*/

//按照格式输入两个整数，范围，中间用“,”分隔，交换。把两个整数按格式输出，中间用“,”分隔。

/*
int main()
{
	int a, b = 0;
	scanf("a=%d,b=%d", &a, &b);
	int tmp = a;
	a = b;
	b = tmp;
	printf("a=%d,b=%d", a, b);
	return 0;
}*/

//反向输出一个四位数
int main()
{
	int a = 0;
	scanf("%d", &a);
	while (a)
	{
		int m = a % 10;
		a = a / 10;
		printf("%d", m);
	}
	
}