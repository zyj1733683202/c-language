#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
/*
int main()
{
	int	arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
	//定义一个数组整型，最多存放10个元素
	//char ch[20];
	//float arr2[5];
	//arr[下标]
	int i = 0;
	while (i < 10)
	{
		printf("%d ",arr[i]);
		i++;
	}
	//printf("%d\n",arr[4]);
	return 0;
}*/
/*
int main() 
{
	//移（2进制）位操作符
	//<<左移
	//>>右移
	int a = 1;
	//整型1占4个字节=32bit位
	//	  0000000000000000000000000000001
	//变为0000000000000000000000000000100
	int b = a << 2;
	printf("%d\n", b);
	return 0;
}*/
//位操作符
/* 
int main()
{
	//(2进制)位操作
	//&按位与
	//|按位或
	//^按位异或
	int a = 3;// 011 
	int b = 5;// 101
	int c = a ^ b;
	printf("%d\n", c);
	return 0;
}
*/
/*
int main()
{
	int a = 10;
	int b = 20;
	a + b;//+  双目操作符
	return 0;
}*/
/*
int main()
{
	int arr[10] = { 0 };
	//共有十个int整型sizeof为40
	//sizeof(arr[0])为4
	int sz = 0;
	//10*szieof( int )=40
	printf("%d\n", sizeof(arr));
	//计算数组的元素个数
	//个数=数组总大小/每个元素的大小
	sz = sizeof(arr) / sizeof(arr[0]);
	printf("sz = s%d\n", sz);
	return 0;
}*/
/*
int main()
{
	int a = 10;
	int arr[] = { 1,2,3,4,5,6 };//6*4=24
	printf("%d\n", sizeof(a));//4
	printf("%d\n", sizeof(int));//4
	printf("%d\n", sizeof a);//4
		//printf("%d\n", sizeof int);//error
	printf("%d\n", sizeof(arr));//计算数组大小，单位是字节
	printf("%d\n", sizeof(arr)/sizeof(arr[0]));//6
	return 0;
}
*/
//取反

/* 只要是整数，内存中存储的都是二进制的补码
* 正数--原码，反码，补码相同
* 负数：
* 原码						--->反码			--->补码
* 直接按照正负			原码的符号位不变		反码+1
* 写出的二进制序列		其他位按位取反得到
* -2
* 10000000000000000000000000000010	-	原码
* 11111111111111111111111111111101	-	反码
* 11111111111111111111111111111110	-	补码*/

int main() 
	{

		int a = 5;//4字节，32bit位
		 //原码10000000000000000000000000000010，反码11111111111111111111111111111101
		 //但定义时计算机存储的是补码即11111111111111111111111111111110
		 //~a=00000000000000000000000000000001
		int b = ~a;//b是有符号的整型 
		printf("%d\n", b);//使用的，打印的是这个数的原码
		return 0;
}
/*
int main()
{
	int a = 0 ;//4字节，32bit位
	int b = ~a;//b是有符号的整型
	 
	//~---按（2进制）位取反，取反都变
	//00000000000000000000000000000000
	//11111111111111111111111111111111
	// 	   求出反码
		//二进制最高一位表示符号位0为正1为负

	//负数在内存中存储的时候，存储的是二进制的补码，显示打印的是原码
	//11111111111111111111111111111111//补码
	//11111111111111111111111111111110//反码
	//10000000000000000000000000000001//原码 

	//1010--0101
	printf("%d\n", b);//使用的，打印的是这个数的原码
	return 0;
}*/
/*
	计算题与C语言无关：2^31+2^30+.......+2^0=
	设a=2^31+2^30+.......+2^0
	则2a=2^32+2^31+.......+2^0
	a=2a-a=2^32-1
	 */
//单目运算符
/*
int main() 
{
	int a = 10;
	int b = ++a;//前置置++
	//a先+1，再赋值给了b
	printf("a = %d b = %d\n", a, b);
	return 0;
}
*/
//强制转换
/*
int main()
{
	int a =(int)3.14;//double->int
	//(类型)，括号里面放个类型叫强制类型转换
	return 0;
}*/
//逻辑运算符
/*
int main()
{
	//真	-	非0
	//假	-	0
	//&&	-	逻辑与
	//||	-	逻辑或
	int a = 3;
	int b = 5;
	//int c = a && b;//只要a和b有一个为假则c为假
	int c = a || b;
	printf("c = %d\n", c);
	return 0;
}
*/
//条件操作符
/*
int main()
{
	int a = 10;
	int b = 20;
	int max = 0;

	max = (a > b ? a : b);//三目操作符
	//a>b则执行a
	//a<b则执行b
		//	就等于以下代码
	//if (a > b)
	//	max = a;
	//else
	//	max = b;
	printf("%d\n", max);
	return 0;
}*/
/*
int Add(int x, int y)
{
	int z = 0;
	z = x + y;
	return z;
}
int main()
{
	//int arr[10] = { 0 };
	//arr[4];//[]-下标引用操作符
	int a = 10;
	int b = 20;
	int sum = Add(a, b);//"()"--函数调用操作符
	printf("%d\n", sum);
	return 0;
}*/