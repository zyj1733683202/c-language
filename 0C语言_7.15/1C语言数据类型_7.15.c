/*
数据类型

char		//字符数据类型
short		//短整型
int			//整型
long		//长整型
longlong	//更长的整型
float		//单精度浮点数
double		//双精度浮点数
*/
#include<stdio.h>

int main()
{
	char ch = 'A';//内存申请
	printf("%c\n", ch);//%c-打印字符格式的数据
	//以字符的形式打印ch
	//20;
	return 0;
}


